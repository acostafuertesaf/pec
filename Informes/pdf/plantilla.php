<?php
	
	require 'fpdf/fpdf.php';
	
	class PDF extends FPDF
	{
		function Header()
		{
			$this->Image('../../recursos/img/pec5.png', 85, 15,50 );
			$this->SetFont('Times','B',15);

			$this->Cell(195,0, 'Informe Usuarios PEC',0,0,'C');
			$this->Ln(60);
		}
		
		function Footer()
		{
			$this->SetY(-15);
			$this->SetFont('Times','I', 8);
			$this->Cell(0,10, 'Pagina '.$this->PageNo().'/{nb}',0,0,'C' );
		}		
	}
?>