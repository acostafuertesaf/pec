<?php
include 'plantilla.php';
require 'conexion.php';

$query = "
    select 
        nom1
        ,ape1
        ,email
        ,usuario
        ,acumulado
        ,tiempototal
        ,fecha
    from usuario
    where acumulado is not null
    order by acumulado desc";

$result = $mysqli->query($query);



$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();

$pdf->SetFillColor(232, 232, 232);
$pdf->SetFont('Arial','B', 11);



$pdf->Cell(10, 6, 'Posc', 1, 0, 'C', 1);
$pdf->Cell(25, 6, 'Nombre', 1, 0, 'C', 1);
$pdf->Cell(25, 6, 'Apellido', 1, 0, 'C', 1);
$pdf->Cell(45, 6, 'Correo', 1, 0, 'C', 1);
$pdf->Cell(25, 6, 'Puntaje', 1, 0, 'C', 1);
$pdf->Cell(25, 6, 'Tiempo', 1, 0, 'C', 1);
$pdf->Cell(40, 6, 'Fecha', 1, 1, 'C', 1);




$pdf->SetFont('Arial','', 11);
$posc = 0;
while($row = $result->fetch_assoc()) {
   
    $pdf->Cell(10,6,$posc += 1, 1, 0, 'C', 1);
    $pdf->Cell(25,6,$row['nom1'], 1, 0, 'C', 1);
    $pdf->Cell(25,6,$row['ape1'], 1, 0, 'C', 1);
    $pdf->Cell(45,6,$row['email'], 1, 0, 'C', 1);
    $pdf->Cell(25,6,$row['acumulado'], 1, 0, 'C', 1);
    $pdf->Cell(25,6,$row['tiempototal'], 1, 0, 'C', 1);
    $pdf->Cell(40,6,$row['fecha'], 1, 1, 'C', 1);
}

$pdf->Output();
