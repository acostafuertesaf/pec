<?php
header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=Reporte Usuarios_".date('Y-m-d').".doc");
include("../../informes/pdf/conexion.php");

$consulta = "select * from usuario where acumulado is not null order by acumulado desc";
$resultado = mysqli_query($mysqli, $consulta);
?>
<center>
<div style="background-color:#4CAF50; color:#fff; text-align:center;">
<h2>REPORTE DE USUARIOS</h2>
</div>
<table border="1">
    <tr>
        <th style="background-color:#4CAF50; color:#fff; text-align:center;"> Posc </th>
        <th style="background-color:#4CAF50; color:#fff; text-align:center;"> Nombre </th>
        <th style="background-color:#4CAF50; color:#fff; text-align:center;"> Apellido </th>
        <th style="background-color:#4CAF50; color:#fff; text-align:center;"> Email </th>
        <th style="background-color:#4CAF50; color:#fff; text-align:center;"> Acumulado </th>
        <th style="background-color:#4CAF50; color:#fff; text-align:center;"> Tiempo </th>
        <th style="background-color:#4CAF50; color:#fff; text-align:center;"> Fecha </th>
       
    </tr>

     <?php
        $posc = 0;
        if(mysqli_num_rows($resultado) != ""){
        while($filas= mysqli_fetch_array($resultado)){ 
        ?>
         <tr>
                <td style="text-align: center;"><?php  echo $posc += 1 ?></td>
                <td style="text-align: center;"><?php  echo $filas['nom1']; ?></td>
                <td style="text-align: center;"><?php echo $filas['ape1']; ?></td>
                <td style="text-align: center;"><?php echo $filas['email']; ?></td>
                <td style="text-align: center;"><?php echo $filas['acumulado']; ?></td>
                <td style="text-align: center;"><?php echo $filas['tiempototal']; ?></td>
                <td style="text-align: center;"><?php echo $filas['fecha']; ?></td>
        </tr>
       
    
    <?php } } ?>
</table>
</center>