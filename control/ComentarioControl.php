<?php

require_once '../modelo/Comentario.php';

class ComentarioControl
{
  function __construct()
  {
    require_once '../conexion/Db.php';
    try {
      $this->pdo = Db::conectar();
    } catch (Exception $ex) {
      die($ex->getMessage());
    }
  }

  public function obtenerTodos()
  {
    //se instancia la clase para traer la categoria(la descripción)
    try {
      $sql = "select * from comentario ORDER BY `nombre` ASC LIMIT 1000";
      $prep = $this->pdo->prepare($sql);
      $prep->execute();
      //se cambia el objeto por todo este codigo
      $comentarios = [];
      foreach ($prep->fetchAll(PDO::FETCH_OBJ) as $com) {
        $comentario = new Comentario();
        $comentario->setIdcomentario($com->idcomentario);
        $comentario->setNombre($com->nombre);
        $comentario->setCorreo($com->correo);
        $comentario->setMensaje($com->mensaje);
            
        array_push($comentarios, $comentario);
      }
      return $comentarios;

      //se rremplaza esta linea por todas las anteriores
      //return $prep->fetchAll(PDO::FETCH_OBJ);
    } catch (Exception $ex) {
      die($ex->getMessage());
    }
  }

  public function obtenerPorId($idcomentario)
  {
    
    try {
      $sql = "select * from comentario where idcomentario = ?";
      $prep = $this->pdo->prepare($sql);
      $prep->execute(array($idcomentario));
      $com = $prep->fetch(PDO::FETCH_OBJ);
      //se cambia el objeto por todo este codigo
      $comentario = new Comentario();
      $comentario->setIdcomentario($com->idcomentario);
      $comentario->setNombre($com->nombre);
      $comentario->setCorreo($com->correo);
      $comentario->setMensaje($com->mensaje);
      
      return $comentario;

      //se rremplaza esta linea por todas las anteriores
      //return $prep->fetchAll(PDO::FETCH_OBJ);
    } catch (Exception $ex) {
      die($ex->getMessage());
    }
  }
  public function agregar($comentario)
  {
    try {
      $sql = "insert into comentario (nombre, email, comentario, fecha) values (?,?,?, now())";
      $this->pdo->prepare($sql)->execute(array(
        $comentario->getNombre(),
        $comentario->getEmail(),
        $comentario->getComentario(),
        
      ));
    } catch (Exception $ex) {
      die($ex->getMessage());
    }
  }
  
  
  
}
