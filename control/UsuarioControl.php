<?php

require_once '../modelo/Usuario.php';

class UsuarioControl
{
  function __construct()
  {
    require_once '../conexion/Db.php';
    try {
      $this->pdo = Db::conectar();
    } catch (Exception $ex) {
      die($ex->getMessage());
    }
  }

  public function obtenerTodos()
  {
    //se instancia la clase para traer la categoria(la descripción)
    try {
      $sql = "SELECT  nom1, ape1, acumulado, tiempototal, fecha  FROM usuario where acumulado is not null ORDER BY acumulado DESC LIMIT 1000";
      $prep = $this->pdo->prepare($sql);
      $prep->execute();
      //se cambia el objeto por todo este codigo
      $usuarios = [];
      foreach ($prep->fetchAll(PDO::FETCH_OBJ) as $usu) {
        $usuario = new Usuario();
        $usuario->setNom1($usu->nom1);
        $usuario->setApe1($usu->ape1);
        $usuario->setAcumulado($usu->acumulado);    
        $usuario->setTiempototal($usu->tiempototal);    
        $usuario->setFecha($usu->fecha);    
        
        array_push($usuarios, $usuario);
      }
      return $usuarios;

      //se remplaza esta linea por todas las anteriores
      //return $prep->fetchAll(PDO::FETCH_OBJ);
    } catch (Exception $ex) {
      die($ex->getMessage());
    }
  }

  public function obtenerPorId($idusuario)
  {
    $perfilControl = new PerfilControl();
    try {
      $sql = "select * from usuario where idusuario = ?";
      $prep = $this->pdo->prepare($sql);
      $prep->execute(array($idusuario));
      $usu = $prep->fetch(PDO::FETCH_OBJ);
      //se cambia el objeto por todo este codigo
      $usuario = new Usuario();
      $usuario->setIdusuario($usu->idusuario);
      $usuario->setNom1($usu->nom1);
      $usuario->setNom2($usu->nom2);
      $usuario->setApe1($usu->ape1);
      $usuario->setApe2($usu->ape2);
      $usuario->setEmail($usu->email);
      $usuario->setUsuario($usu->usuario);
      $usuario->setAcumulado($usu->acumulado);
      $usuario->setPassword($usu->password);
      return $usuario;

      //se rremplaza esta linea por todas las anteriores
      //return $prep->fetchAll(PDO::FETCH_OBJ);
    } catch (Exception $ex) {
      die($ex->getMessage());
    }
  }


  public function agregar($usuario)
  {
    try {
      $sql = "insert into usuario (idusuario, nom1, nom2, ape1, ape2, email, usuario, password) values (uuid(),?,?,?,?,?,?,?)";
      $this->pdo->prepare($sql)->execute(array(
      
        $usuario->getNom1(),
        $usuario->getNom2(),
        $usuario->getApe1(),
        $usuario->getApe2(),
        $usuario->getEmail(),
        $usuario->getUsuario(),
        $usuario->getPassword(),
      ));
    } catch (Exception $ex) {
      die($ex->getMessage());
    }
  }


  public function modificar($usuario) {
    try {
      $sql = "update usuario set acumulado=?, tiempototal=?, fecha=now() where idusuario=?";
      $this->pdo->prepare($sql)->execute(array(
          $usuario->getAcumulado(),
          $usuario->getTiempototal(),  
          $usuario->getIdusuario()      
        ));
        
    } catch (Exception $ex) {
      die($ex->getMessage());
    }
  }
  
  
  
}