<?php
require_once '../modelo/Usuario.php';
require_once '../control/UsuarioControl.php';
$usuario = new Usuario();



$usuario->setNom1($_REQUEST['nom1']);
$usuario->setNom2($_REQUEST['nom2']);
$usuario->setApe1($_REQUEST['ape1']);
$usuario->setApe2($_REQUEST['ape2']);
$usuario->setEmail($_REQUEST['email']);
$usuario->setUsuario($_REQUEST['usuario']);
$usuario->setAcumulado($_REQUEST['acumulado']);
$usuario->setPassword($_REQUEST['password']);
$usuario->setTiempototal($_REQUEST['tiempototal']);
$usuario->setFecha($_REQUEST['fecha']);

$usuarioControl = new UsuarioControl();

if ($usuario->getUsuario() <> null) {
    $usuarioControl->agregar($usuario);
}


header('Location: ../vista/login.php');
?>
