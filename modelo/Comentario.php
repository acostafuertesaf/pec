<?php

class Comentario
{
    
    private $nombre;
    private $email;
    private $comentario;
    private $fecha;
    



    function __construct()
    { }

    ///GETS


    function getNombre()
    {
        return $this->nombre;
    }

    function getEmail()
    {
        return $this->email;
    }

    function getComentario()
    {
        return $this->comentario;
    }

    function getFecha()
    {
        return $this->fecha;
    }


    //SETS


    function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    function setEmail($email)
    {
        $this->email = $email;
    }

    function setComentario($comentario)
    {
        $this->comentario = $comentario;
    }

    function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    

    
}
