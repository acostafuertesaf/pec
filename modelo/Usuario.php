<?php

class Usuario
{
    private $idusuario;
    private $nom1;
    private $nom2;
    private $ape1;
    private $ape2;
    private $email;
    private $usuario;
    private $acumulado;
    private $password;
    private $tiempototal;
    private $fecha;

    function __construct()
    { }

    ///GETS

    function getIdusuario()
    {
        return $this->idusuario;
    }

    function getNom1()
    {
        return $this->nom1;
    }


    function getNom2()
    {
        return $this->nom2;
    }
    function getApe1()
    {
        return $this->ape1;
    }

    function getApe2()
    {
        return $this->ape2;
    }

    function getEmail()
    {
        return $this->email;
    }


    function getUsuario()
    {
        return $this->usuario;
    }

    function getAcumulado()
    {
        return $this->acumulado;
    }

    function getPassword()
    {
        return $this->password;
    }

    function getTiempototal()
    {
        return $this->tiempototal;
    }

    function getFecha()
    {
        return $this->fecha;
    }

    //SETS

    function setIdusuario($idusuario)
    {
        $this->idusuario = $idusuario;
    }

    function setNom1($nom1)
    {
        $this->nom1 = $nom1;
    }

    function setNom2($nom2)
    {
        $this->nom2 = $nom2;
    }

    function setApe1($ape1)
    {
        $this->ape1 = $ape1;
    }

    function setApe2($ape2)
    {
        $this->ape2 = $ape2;
    }

    function setEmail($email)
    {
        $this->email = $email;
    }

    function setUsuario($usuario)
    {
        $this->usuario = $usuario;
    }

    function setAcumulado($acumulado)
    {
        $this->acumulado = $acumulado;
    }

    function setPassword($password)
    {
        $this->password = $password;
    }

    function setTiempototal($tiempototal)
    {
        $this->tiempototal = $tiempototal;
    }

    function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }
}
