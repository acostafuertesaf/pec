
$('#oculMenu').click(e => {
    $('#menu').css('display', 'none');
    setTimeout(() => {
        $('#menuHam').css('display', 'block');
    }, 500);


});

function ampliar(opc) {
    debugger;
    switch (opc) {
        case 'Tiro':
            if ($(".btnTiro").html() == 'Ampliar') {
                $(".btnTiro").html('Minimizar');
                $("#ampliarTiro").css('display', 'block')
            } else {
                $(".btnTiro").html('Ampliar');
                $("#ampliarTiro").css('display', 'none')
            }
            break;

        case 'Vertical':
            if ($(".btnVertical").html() == 'Ampliar') {
                $(".btnVertical").html('Minimizar');
                $("#ampliarVertical").css('display', 'block')
            } else {
                $(".btnVertical").html('Ampliar');
                $("#ampliarVertical").css('display', 'none')
            }
            break;


    }
}

function ejm(opc) {
    debugger;
    //$(`#ejm${opc}`).css('display', 'block');
    switch (opc) {
        case 'Tiro':
            if ($(".btnEjmTiro").html() == 'Ver ejemplo') {
                $(".btnEjmTiro").html('Minimizar ejemplo');
                $(`#ejm${opc}`).css('display', 'block');
            } else {
                $(".btnEjmTiro").html('Ver ejemplo');
                $(`#ejm${opc}`).css('display', 'none');
            }
            break;

        case 'Vertical':
            if ($(".btnEjmVertical").html() == 'Ver ejemplo') {
                $(".btnEjmVertical").html('Minimizar ejemplo');
                $(`#ejm${opc}`).css('display', 'block');
            } else {
                $(".btnEjmVertical").html('Ver ejemplo');
                $(`#ejm${opc}`).css('display', 'none');
            }
            break;


    }
}

function w3_open() {
    $('#menuHam').css('display', 'none');
    setTimeout(() => {
        //document.getElementById("mySidebar").style.display = "block";
        //document.getElementById("myOverlay").style.display = "block";
        $('#menu').css('display', 'block');

    }, 500);
}

function w3_close() {
    document.getElementById("mySidebar").style.display = "none";
    document.getElementById("myOverlay").style.display = "none";
}

// Modal Image Gallery
function onClick(element) {
    document.getElementById("img01").src = element.src;
    document.getElementById("modal01").style.display = "block";
    var captionText = document.getElementById("caption");
    captionText.innerHTML = element.alt;
}


$(document).ready(function () {

    $('.ir-arriba').click(function () {
        $('body, html').animate({
            scrollTop: '0px'
        }, 300);
    });

    $(window).scroll(function () {
        if ($(this).scrollTop() > 0) {
            $('.ir-arriba').slideDown(300);
        } else {
            $('.ir-arriba').slideUp(300);
        }
    });

});

function historia(hist) {
    $(`#${hist}`).css('display', 'block');
}

function noHistoria(hist) {
    $(`#${hist}`).css('display', 'none');
}
