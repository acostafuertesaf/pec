function validar() {

  let validado = true;


  let msgs = document.querySelectorAll('small[id^="msg"]');
  msgs.forEach(el => {
    el.innerHTML = "";
  });

  //1. Obteber los objetos de los controles y etiquetas de HTML
  let nom1 = document.querySelector('#nom1');
  let ape1 = document.querySelector('#ape1');
  let email = document.querySelector('#email');
  let usuario = document.querySelector('#usuario');
  let password = document.querySelector('#password');


  let msgNom1 = document.querySelector('#msgNom1');
  let msgApe1 = document.querySelector('#msgApe1');
  let msgEmail = document.querySelector('#msgEmail');
  let msgUsuario = document.querySelector('#msgUsuario');
  let msgPassword = document.querySelector('#msgPassword');


  //2. Si son cajas de texto o textAreas y vamos a validar, se suguere utilizar regex
  //let rgxNom1 = /^[a-zA-Z Á]{2,30}$/;
  //let rgxApe2 = /^[a-zA-Z Á]{2,30}$/;
  let rgxUsuario = /^[a-zA-Z Á]{2,30}$/;
  let rgxPassword = /(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{4,20})$/;
  let rgxEmail = /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;


  //4. Preguntas de validación
  if (nom1.value.trim() == "" || nom1.value.length == 0) {
    msgNom1.innerHTML += "El nombre no puede enviarse vacio !!<br/>";
    validado = false;
  }
  /*if (!rgxNom1.test(nom1.value)) {
    msgNom1.innerHTML += "El nombre debe tener mínimo tres caracteres !!";
    validado = false;
  }*/
  if (usuario.value.trim() == "" || usuario.value.length == 0) {
    msgUsuario.innerHTML += "El usuario no puede enviarse vacio !!<br/>";
    validado = false;
  }
  if (!rgxUsuario.test(usuario.value)) {
    msgUsuario.innerHTML += "El usuario debe tener mínimo tres caracteres !!";
    validado = false;
  }
  if (ape1.value.trim() == "" || ape1.value.length == 0) {
    msgApe1.innerHTML += "El apellido no puede enviarse vacio !!<br/>";
    validado = false;
  }
  /*if (!rgxApe1.test(ape1.value)) {
    msgApe1.innerHTML += "El apellido debe tener mínimo tres caracteres !!";
    validado = false;
  }*/
  if (!rgxPassword.test(password.value)) {
    msgPassword.innerHTML = "El password no es valido !!";
    validado = false;
  }
  if (!rgxEmail.test(email.value)) {
    msgEmail.innerHTML = "Por favor ingrese un Email valido !!";
    validado = false;
  }
  return validado;

}


