<?php
$id = $_GET['id'];
$nom = $_GET['nom'];
$ape = $_GET['ape'];

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../recursos/cssLibrerias/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="icon" type="image/png" href="../recursos/img/pec5.png" />
    <title><?php print $nom; ?> <?php print $ape; ?> Home Juego</title>
    <style>
        .btnm10 {
            margin-right: 20px;
        }

        .btnHover:hover {
            background-color: #0D72B5;
        }

        #dropzone {
            top: 150px;
            width: 200px;
            height: 200px;
            background: cyan;
            border: solid 1px;
        }

        .btnStyle {
            border-color: #2e75b6;
            border-width: medium;
            border: 2px blue solid;
            border-radius: 5px;
        }

        .btnStyle:hover {
            border-color: #B0DEFF;
            border-width: medium;
        }

        footer {
            width: 100%;
            position: absolute;
            bottom: 0;
            color: #000;
        }

        html {
            min-height: 100%;
            position: relative;
        }

        .cards-list {
            z-index: 0;
            width: 100%;
            display: flex;
            justify-content: space-around;
            flex-wrap: wrap;
            margin-top: 6cm;
        }

        .card {
            margin: 30px auto;
            width: 300px;
            height: 300px;
            border-radius: 40px;
            box-shadow: 5px 5px 30px 7px rgba(0, 0, 0, 0.25), -5px -5px 30px 7px rgba(0, 0, 0, 0.22);
            cursor: pointer;
            transition: 0.4s;

        }

        .card .card_image {
            width: inherit;
            height: inherit;
            border-radius: 40px;
        }

        .card .card_image img {
            width: inherit;
            height: inherit;
            border-radius: 40px;
            object-fit: cover;
        }

        .card .card_title {
            text-align: center;
            border-radius: 0px 0px 40px 40px;
            font-family: sans-serif;
            font-weight: bold;
            font-size: 20px;
            margin-top: -50px;
            height: 40px;
        }

        .card:hover {
            transform: scale(0.9, 0.9);
            box-shadow: 5px 5px 30px 15px rgba(0, 0, 0, 0.25),
                -5px -5px 30px 15px rgba(0, 0, 0, 0.22);

        }

        @media all and (max-width: 500px) {
            .card-list {
                flex-direction: column;
            }
        }

        #snackbar,
        #snackbar1-4,
        #snackbar1-5,
        #snackbar1-7,
        #snackbar1-8,
        #snackbar1-9,
        #snackbar1-10,
        #snackbar1-11,
        #snackbar2-2,
        #snackbar2-3,
        #snackbar2-4,
        #snackbar2-5,
        #snackbar2-6,
        #snackbar2-7,
        #snackbar2-8,
        #snackbar2-9,
        #snackbar2-10 {
            visibility: hidden;
            min-width: 400px;
            margin-bottom: 45%;
            color: #000;
            font-weight: bold;
            text-align: center;
            border-radius: 5px;
            padding: 16px;
            position: fixed;
            z-index: 1;
            left: 50%;
            bottom: 30px;
            font-size: 25px;
        }

        #snackbar.show,
        #snackbar1-4.show,
        #snackbar1-5.show,
        #snackbar1-7.show,
        #snackbar1-8.show,
        #snackbar1-9.show,
        #snackbar1-10.show,
        #snackbar1-11.show,
        #snackbar2-2.show,
        #snackbar2-3.show,
        #snackbar2-4.show,
        #snackbar2-5.show,
        #snackbar2-6.show,
        #snackbar2-7.show,
        #snackbar2-8.show,
        #snackbar2-9.show,
        #snackbar2-10.show {
            visibility: visible;
            -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
            animation: fadein 0.5s, fadeout 0.5s 2.5s;
        }



        @-webkit-keyframes fadein {
            from {
                bottom: 0;
                opacity: 0;
            }

            to {
                bottom: 30px;
                opacity: 1;
            }
        }

        @keyframes fadein {
            from {
                bottom: 0;
                opacity: 0;
            }

            to {
                bottom: 30px;
                opacity: 1;
            }
        }

        @-webkit-keyframes fadeout {
            from {
                bottom: 30px;
                opacity: 1;
            }

            to {
                bottom: 0;
                opacity: 0;
            }
        }

        @keyframes fadeout {
            from {
                bottom: 30px;
                opacity: 1;
            }

            to {
                bottom: 0;
                opacity: 0;
            }
        }

        .btnAux {
            width: 150px;
            height: 50px;
            font-size: 18px;
            font-weight: bold;
            margin-bottom: 10px;
        }
    </style>
</head>
<script src="../recursos/js/script.js"></script>

<body style="background: linear-gradient(#2e75b6, #B0DEFF, white); ">


    <header class="w3-container w3-top  w3-xlarge w3-padding-16" style="height: 70px;">
        <img src="../recursos/img/pec.png" class="rounded-circle w3-left" alt="logo" style="width:80px; height:80px">
        <span class="w3-right w3-padding w3-opacity" style="font-size: 25px; color: white; font-weight: bold;">Profesor de Ecuaciones Cuadráticas</span>
        <a href="home.php?nom=<?php print $nom; ?>&ape=<?php print $ape; ?>&id=<?php print $id; ?>" class="w3-right w3-padding return w3-opacity" style="font-size: 22px; font-weight: bold; text-decoration: none; color: white"><span><img src="../recursos/img/regresar.png" style="width: 25px;" />Regresar</span></a>
        <div class="row" style="margin-left: 1cm">
    </header>



    <tbody>
        <div class="cards-list">
            <button class="card 1 btnEjerPisc" id="btn">

                <div class="card_image"> <img src="../recursos/gifs/piscina.gif" /> </div>
                <div class="card_title title-white">
                    <div></div><br><br><span></span> <span><img src="../recursos/img/click-black.png" style="width: 40px;" /></span>Ejercicio de la piscina
                </div>
            </button>

            <button class="card 2 btnEjerTel" id="btn2" style="background-color: #ff9900;">
                <div class="card_image">
                    <img src="../recursos/gifs/tel.gif" />

                </div>
                <div class="card_title title-white">
                    <div></div><br><br><span><img src="../recursos/img/click-black.png" style="width: 40px;" /></span>Ejercicio del televisor
                </div>
            </button>

            <button class="card 3" id="juegoFinal" style="background-color: #000;">
                <div class="card_image">
                    <img src="../recursos/gifs/quien.gif" />
                </div>
                <div class="card_title title-white">
                    <div></div><br><br> <span><img src="../recursos/img/click-black.png" style="width: 40px;" /></span>Quien quiere ser Matemático
                </div>
            </button>
        </div>

        <div id="snackbar"><label id="alert"></label></div>
    </tbody>

    <!-- The Modal Ejercicio 1 -->
    <div class="modal fade" id="modalEjer1">
        <div class="modal-dialog modal-xl">
            <div class="modal-content" style="height: 800px;">

                <!-- Modal Header -->
                <div class="modal-header" style="height: 70px;">
                    <div class="progress col-sm-9" style="height: 30px;">
                        <div class="progress-bar" id="progress1">0%</div>
                    </div>
                    <div class="col-sm-1">
                        <div class="row">
                            <span style="font-weight: bold;">Pts:</span>&nbsp;&nbsp;<span><input style="width: 35px;" id="pts" disabled /></span>
                        </div>
                    </div>

                </div>
                <!-- Modal body -->
                <div class="modal-body ">


                    <!-- modal 1 Encabezado -->
                    <div style="display: block" id="Ejer1Modal1" class="container">
                        <center>
                            <h3 style="text-align: center; margin-top: 150px" class="col-sm-9">
                                Si tenemos una piscina de 15 m de largo, por 9 m de ancho, y queremos construir un camino de cemento alrededor de esta con un
                                ancho uniforme, además se quiere que el área del camino sea de 81 m2 ¿Cuál debe ser el ancho del camino?
                            </h3>
                        </center>
                    </div>

                    <!-- modal 2 -->
                    <div style="display: none" id="Ejer1Modal2" class="container">
                        <div class="row">
                            <div class="col-sm-7">
                                <img src="../recursos/img/piscinas/pisc1.jpg" style="width:800px; height:500px">
                            </div>

                            <div class="col-sm-5" style="margin-top: 100px">
                                <h3 style="text-align: center;">Como el ancho debe ser uniforme, podemos llamar el ancho del camino con nuestra variable X, el Cual
                                    es nuestro valor a encontrar.
                                </h3>
                            </div>
                        </div>
                    </div>

                    <!-- modal 3 -->
                    <div style="display: none" id="Ejer1Modal3" class="container">
                        <div class="row" style="margin-top: 50px">
                            <div class="col-sm-7">
                                <img src="../recursos/img/piscinas/pisc3.jpg" style="width:600px; height:400px">
                            </div>

                            <div class="col-sm-5" style="margin-top: 100px">
                                <h3 style="text-align: center;">Con esta información ya podríamos establecer cuál puede ser el largo y el ancho del rectángulo más grande,
                                    el cual incluye el camino.
                                </h3>
                            </div>
                        </div>
                    </div>

                    <!-- modal 4 -->
                    <div style="display: none" id="Ejer1Modal4" class="container">

                        <div class="row">

                            <div class="col-sm-7" style="margin-top: 30px">
                                <div id="res" style="background-color: #E6E6E6; height: 50px; margin-left: 100px;"></div>
                                <img src="../recursos/img/piscinas/pisc3.jpg" style="width:600px; height:400px">
                            </div>

                            <div class="col-sm-5" style="margin-top: 80px">
                                <div class="row">
                                    <h3 style="text-align: center;">
                                        De acuerdo a lo aprendido, ¿Cuál crees que pueda ser el ancho del rectángulo grande?<br>
                                    </h3>
                                </div><br><br>

                                <div style="margin-left: 180px;">
                                    <center>
                                        <div class="row">
                                            <button class="btnStyle" onclick="ejer1Incorrecta(4)"> <img src="../recursos/img/piscinas/formulas/modal4/opc1.png"></button>
                                        </div><br>
                                        <div class="row">
                                            <button class="btnStyle" onclick="ejer1Correcta(4)"> <img src="../recursos/img/piscinas/formulas/modal4/correcta.png"></button>
                                        </div><br>
                                        <div class="row">
                                            <button class="btnStyle" onclick="ejer1Incorrecta(4)"> <img src="../recursos/img/piscinas/formulas/modal4/opc2.png"></base_convert>
                                        </div>
                                    </center>
                                </div>

                            </div>
                        </div><br>
                        <div id="snackbar1-4"><label class="alert2"></label></div>
                    </div>

                    <!-- modal 5 -->
                    <div style="display: none" id="Ejer1Modal5" class="container">
                        <div class="row">
                            <div class="col-sm-7">
                                <img src="../recursos/img/piscinas/pisc5.jpg" style="width:600px; height:400px">
                            </div>

                            <div class="col-sm-5" style="margin-top: 80px">
                                <div class="row">
                                    <h3 style="text-align: center">
                                        Ahora, ¿Cuál crees que pueda ser la altura del rectángulo?
                                    </h3>
                                </div>
                                <br><br>
                                <div style="margin-left: 150px;">
                                    <div><button class="btnStyle" onclick="ejer1Correcta(5)"><img class="mod4opc1" src="../recursos/img/piscinas/formulas/modal5/correctanew.png"></button></div><br>
                                    <div><button class="btnStyle" onclick="ejer1Incorrecta(5)"><img class="mod4opc1" src="../recursos/img/piscinas/formulas/modal5/opc1new.png"></button></div><br>
                                    <div><button class="btnStyle" onclick="ejer1Incorrecta(5)"><img class="mod4opc1" src="../recursos/img/piscinas/formulas/modal5/opc2new.png"></button></div><br>
                                </div>
                            </div>
                        </div><br><br><br>
                        <div id="snackbar1-5"><label class="alert2"></label></div>
                    </div>

                    <!-- modal 6 -->
                    <div style="display: none" id="Ejer1Modal6" class="container">
                        <div class="row" style="text-align: center;">
                            <h3> Como tenemos la información del área del camino, que es de 81 m2, podemos hacer un planteamiento en el que se quiere hallar
                                el área del rectángulo grande, al cual le podemos restar el área de la piscina y se obtendría el área del camino. </h3>
                        </div>
                        <br><br>
                        <div class="row col-sm-12" style="text-align: center; margin-left: 230px;">
                            <h4>
                                (Área Rectángulo Grande)-(Área piscina) = Área camino<br>
                                (Área Rectángulo Grande)-(Área piscina) = 81
                            </h4>
                        </div>
                        <div class="row">
                            <img src="../recursos/img/piscinas/pisc6.jpg" style="width:600px; height:400px; margin-left: 250px;">
                        </div>
                    </div>

                    <!-- modal 7 -->
                    <div style="display: none" id="Ejer1Modal7" class="container">
                        <div class="row">
                            <div class="col-sm-7">
                                <img src="../recursos/img/piscinas/pisc6.jpg" style="width:600px; height:400px">
                            </div>
                            <div class="col-sm-5" style="margin-top: 80px">
                                <h3>
                                    Comencemos a hallar las Áreas.<br>¿Cuál es el Área del rectángulo grande?
                                </h3><br><br>
                                <div style="margin-left: 80px;">
                                    <div><button class="btnStyle" onclick="ejer1Incorrecta(7)"><img class="mod4opc1" src="../recursos/img/piscinas/formulas/modal7/opc1new.png"></button></div><br>
                                    <div><button class="btnStyle" onclick="ejer1Incorrecta(7)"><img class="mod4opc1" src="../recursos/img/piscinas/formulas/modal7/opc2new.png"></button></div><br>
                                    <div><button class="btnStyle" onclick="ejer1Correcta(7)"><img class="mod4opc1" src="../recursos/img/piscinas/formulas/modal7/correctanew.png"></button></div><br>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="row">
                            &nbsp;&nbsp;&nbsp;
                            <div style="width: 50px; margin-left: 50px; background-color: #777; "></div>- (Área piscina) = 81
                        </div><br> -->

                        <div class="row col-md-8" style="display: none" id="ejer1Mod7Success">
                            <div class="alert alert-success">
                                <strong>Ya puedes continuar!</strong> <img class="mod4opc1" style="width: 150px;margin-left: 15px; margin-bottom: 10px;" src="../recursos/img/piscinas/formulas/modal7/correctanew.png"><span style="font-size: 25px;"> - (Área piscina) = 81</span>
                            </div>
                        </div>
                        <div id="snackbar1-7"><label class="alert2"></label></div>
                    </div>

                    <!-- modal 8 -->
                    <div style="display: none" id="Ejer1Modal8" class="container">
                        <div class="row">
                            <div class="col-sm-7">
                                <img src="../recursos/img/piscinas/pisc6.jpg" style="width:600px; height:400px">
                            </div>
                            <div class="col-sm-5" style="margin-top: 80px">
                                <h3 style="text-align: center">
                                    Ahora ¿Cuál es el Área de la piscina?
                                </h3><br><br>
                                <div style="margin-left: 120px;">
                                    <div><button class="btnStyle" onclick="ejer1Incorrecta(8)"><img class="mod4opc1" src="../recursos/img/piscinas/formulas/modal7/opc1new.png"></button></div><br>
                                    <div><button class="btnStyle" onclick="ejer1Incorrecta(8)"><img class="mod4opc1" src="../recursos/img/piscinas/formulas/modal7/opc2new.png"></button></div><br>
                                    <div><button class="btnStyle" onclick="ejer1Correcta(8)"><img class="mod4opc1" src="../recursos/img/piscinas/formulas/modal8/correctanew.png"></button></div><br>
                                </div>
                            </div>
                        </div><br><br>
                        <div class="row col-md-7" style="display: none" id="ejer1Mod8Success">
                            <div class="alert alert-success">
                                <strong>Ya puedes continuar!</strong>
                                <img class="mod4opc1" style="width: 150px;margin-left: 15px; margin-bottom: 10px;" src="../recursos/img/piscinas/formulas/modal7/correctanew.png"><span style="font-size: 25px;"><b>-</b></span>
                                <img class="mod4opc1" style="width: 65px;margin-left: 5px; margin-bottom: 10px;" src="../recursos/img/piscinas/formulas/modal8/correctanew.png">
                                <span style="font-size: 25px;"> = 81</span>
                            </div>
                        </div>

                        <div id="snackbar1-8"><label class="alert2"></label></div>
                    </div>

                    <!-- modal 9 -->
                    <div style="display: none" id="Ejer1Modal9" class="container">
                        <div class="row" style="margin-top: 40px; margin-left: 300px;">
                            <h2>
                                (15 + 2x)(9 + 2x) - (15 * 9) = 81
                            </h2>
                        </div>
                        <br>
                        <div class="row">
                            <h3 style="text-align: center">
                                Acabamos de pasar de un problema geométrico en el cual involucrábamos el área de rectángulos
                                a un problema algebraico, que nos lleva a trabajar con una ecuación. Ahora, si usamos en la ecuación
                                el producto de binomios (15 + 2x)(9 + 2x) y aplicamos la propiedad distributiva, ¿Cuál será
                                la ecuación resultante?
                            </h3>
                        </div>
                        <br>
                        <div class="row">
                            <div style="margin-left: 400px;">
                                <div><button class="btnStyle" onclick="ejer1Incorrecta(9)"><img class="mod4opc1" src="../recursos/img/piscinas/formulas/modal9/opc1new.png"></button></div><br>
                                <div><button class="btnStyle" onclick="ejer1Incorrecta(9)"><img class="mod4opc1" src="../recursos/img/piscinas/formulas/modal9/opc2new.png"></button></div><br>
                                <div><button class="btnStyle" onclick="ejer1Correcta(9)"><img class="mod4opc1" src="../recursos/img/piscinas/formulas/modal9/correctanew.png"></button></div><br>
                            </div>
                        </div>
                        <div class="row" style="display: none" id="ejer1Mod9Success">
                            <div class="alert alert-success">
                                <strong>Ya puedes continuar!</strong>
                            </div>
                        </div>

                        <div id="snackbar1-9"><label class="alert2"></label></div>
                    </div>

                    <!-- modal 10 -->
                    <div style="display: none" id="Ejer1Modal10" class="container">
                        <div class="row" style="margin-top: 30px; margin-left: 390px;">
                            <h2 style="text-align: center">
                                <span>4x2 + 48x - 81 = 0</span>
                            </h2>
                        </div>
                        <br>
                        <div class="row" style="text-align: center">
                            <h3>
                                Como se puede ver, llegamos a una ecuación cuadrática, la cual
                                podemos resolver usando dos caminos principales, factorización y la fórmula
                                cuadrática. Para este caso es más útil usar la formula cuadrática<br><br>
                                <img src="../recursos/img/piscinas/formulas/modal10/modelo.png"><br><br>
                                Con base en nuestra ecuación. ¿Cuál sería la fórmula
                                cuadrática aplicada correctamente?<br><br>
                                <div>
                                    <button class="btnStyle" onclick="ejer1Incorrecta(10)" style="height: 70px" class=""><img src="../recursos/img/piscinas/formulas/modal10/opcion1new.png"></button>
                                    <button class="btnStyle" onclick="ejer1Incorrecta(10)" style="height: 70px" class=""><img src="../recursos/img/piscinas/formulas/modal10/opcion2new.png"><br></button>
                                    <button class="btnStyle" onclick="ejer1Correcta(10)" style="height: 70px" class=""><img src="../recursos/img/piscinas/formulas/modal10/correctanew.png"><br></button>
                                    <button class="btnStyle" onclick="ejer1Incorrecta(10)" style="height: 70px" class=""><img src="../recursos/img/piscinas/formulas/modal10/opcion3new.png"><br></button>
                                </div>
                            </h3>
                        </div><br>
                        <div class="row" id="ejer1Mod10Success" style="display: none">
                            <div class="alert alert-success">
                                <strong>Ya puedes continuar!</strong>
                            </div>
                        </div>
                        <div id="snackbar1-10"><label class="alert2"></label></div>

                    </div>

                    <!-- modal final -->
                    <div style="display: none" id="Ejer1Modal11">
                        <div class="row" style="text-align: center">
                            <h3>
                                Resolviendo estas operaciones tendremos: <span><img src="../recursos/img/piscinas/formulas/modal11/ecuacion.png"></span>
                            </h3>
                        </div>
                        <br>
                        <div class="row" style="text-align: center">
                            <h3>
                                En este caso tendremos dos posibles soluciones que son:<br>
                            </h3>
                        </div>
                        <br><br>
                        <div class="row">
                            <button class="col-sm-6" class="btnStyle" onclick="ejer1Incorrecta(11)"><img src="../recursos/img/piscinas/formulas/modal11/opcion1new.png"></button>
                            <div class="col-sm-6" style="text-align: center">
                                <h5>Analizando nuestro problema inicial en el cual nos dan un Área de 81m2
                                    y debemos hallar el ancho del camino (x).</h5><br>
                            </div>

                        </div><br>
                        <div class="row">
                            <button class="col-sm-6" class="btnStyle" onclick="ejer1Correcta(11)" id="ejer1Mod11ecu2" style="height: 1.5cm"><img src="../recursos/img/piscinas/formulas/modal11/opcion2new.png"></button>

                            <div class="col-sm-6" style="text-align: center"><b>
                                    <h5>¿Cuál consideras que es la opción correcta?</h5>
                                </b></div>
                        </div><br><br><br>

                        <div class="row" style="display: none" id="ejer1Mod11Success">
                            <div class="alert alert-success">
                                <h4><strong>Correcto,</strong> Has logrado encontrar el ancho del camino de la piscina,
                                    para este caso, la respuesta correcta es que el ancho del camino debe ser de
                                    <strong>1,5 m</strong></h4>
                            </div>
                            <center>
                                <div><img src="../recursos/img/piscinas/formulas/modal12/resFinal.png"></div><br>
                                <button class="btn btn-success" id="finalizarEjer1" style="width: 3cm; height: 1.5cm; font-size: 20px;">Finalizar</button>
                            </center>


                            <!-- <center>
                                <div><img src="../recursos/img/gifs/congratulation.gif"></div>
                            </center> -->
                        </div>
                        <div id="snackbar1-11"><label class="alert2"></label></div>
                    </div>
                    <!-- <div class="chronometer">
                       
                        <div class="buttons">
                            <button class="emerald" onclick="start()">START &#9658;</button>
                            <button class="emerald" onclick="stop()">STOP &#8718;</button>
                            <button class="emerald" onclick="resume()">RESUME &#8634;</button>
                            <button class="emerald" onclick="reset()">RESET &#8635;</button>
                        </div>
                    </div> -->
                </div>
                <!-- Modal footer -->
                <div class="modal-footer" id="Ejer1ModalEnc">
                    <a><img src="../recursos/img/volver.jpg" style="width:80px; height:59px; margin-right: 920px" onclick="volverEjer1()" id="volverEjer1"></a>
                    <button class="btn btn-primary" id="continuarEjer1">Continuar</button>


                </div>
            </div>
        </div>
    </div>

    <!-- The Modal Ejercicio 2 -->
    <div class="modal fade" id="modalEjer2">
        <div class="modal-dialog modal-xl">
            <div class="modal-content" style="height: 800px;">

                <!-- Modal Header -->
                <div class="modal-header" style="height: 70px;">
                    <div class="progress col-sm-9" style="height: 30px;">
                        <div class="progress-bar" id="progress2">0%</div>
                    </div>
                    <div class="col-sm-1">
                        <div class="row">
                            <span style="font-weight: bold;">Pts:</span>&nbsp;&nbsp;<span><input style="width: 35px;" id="pts2" disabled /></span>
                        </div>
                    </div>
                </div>

                <!-- Modal body -->
                <div class="modal-body">

                    <!-- modal 1 Encabezado -->
                    <div style="display: block" id="Ejer2Modal1">
                        <h2 style="text-align: center; margin-top: 80px">
                            El ancho de la pantalla de un televisor tiene 6 cm más que su altura
                            y su diagonal es 12 cm más que su altura. ¿Cuáles son las longitudes de
                            su ancho, alto y diagonal?
                        </h2>
                    </div>

                    <!-- modal 2 -->
                    <div style="display: none" id="Ejer2Modal2">
                        <h2 style="margin-top: 40px; margin-left: 200px;">El primer paso es darles nombres a las incógnitas.</h2>
                        <br>
                        <div class="row">
                            <div class="col-sm-7">
                                <img src="../recursos/img/televisor/tel11.png" style="width:600px; height:400px">
                            </div>
                            <div class="col-sm-5" style="margin-top: 20px;">
                                <div>
                                    <h3 style="text-align: center">¿Cual consideras que puede ser nuestra variable x?</h3>
                                </div><br><br>
                                <center>
                                    <div>
                                        <div><button class="btnStyle btnAux" onclick="ejer2Incorrecta(2)">Ancho</button></div>
                                        <div><button class="btnStyle btnAux" onclick="ejer2Correcta(2)">Altura</button></div>
                                        <div><button class="btnStyle btnAux" onclick="ejer2Incorrecta(2)">Diagonal</button></div>
                                    </div>
                                </center>
                            </div>
                        </div><br>
                        <div class="row" id="ejer2Mod2Success" style="display: none">
                            <div class="alert alert-success">
                                <strong>Ya puedes continuar!</strong>
                            </div>
                        </div>
                        <div id="snackbar2-2"><label class="alert2"></label></div>
                    </div>

                    <!-- modal 3 -->
                    <div style="display: none" id="Ejer2Modal3">
                        <div class="row" style="margin-top: 20px">
                            <div class="col-sm-7">
                                <img src="../recursos/img/televisor/tel2.png" style="width:600px; height:400px">
                            </div>
                            <div class="col-sm-5">
                                <div>
                                    <h3>Con base a nuestro enunciado, ¿Cuál crees que seria el valor del
                                        ancho del televisor?</h3>
                                </div><br>
                                <center>
                                    <div>
                                        <div><button class="btnStyle btnAux" onclick="ejer2Incorrecta(3)">X + 12</button></div>
                                        <div><button class="btnStyle btnAux" onclick="ejer2Incorrecta(3)">(X)2 + 18</button></div>
                                        <div><button class="btnStyle btnAux" onclick="ejer2Incorrecta(3)">18</button></div>
                                        <div><button class="btnStyle btnAux" onclick="ejer2Correcta(3)"><b>X + 6</b></button></div>
                                    </div>
                                </center>
                            </div>
                        </div><br><br>
                        <div class="row" id="ejer2Mod3Success" style="display: none">
                            <div class="alert alert-success">
                                <strong>Ya puedes continuar!</strong>
                            </div>
                        </div>
                        <div id="snackbar2-3"><label class="alert2"></label></div>
                    </div>

                    <!-- modal 4 -->
                    <div style="display: none" id="Ejer2Modal4">
                        <div class="row" style="margin-top: 20px">
                            <div class="col-sm-7">
                                <img src="../recursos/img/televisor/tel3.png" style="width:600px; height:400px">
                            </div>
                            <div class="col-sm-5">
                                <div>
                                    <h3>Muy bien, ahora debemos hallar el valor de la diagonal del televisor,
                                        ¿Cuál sería este valor?
                                    </h3>
                                </div><br>
                                <center>
                                    <div>
                                        <div><button class="btnStyle btnAux" onclick="ejer2Incorrecta(4)">(X)2 + 9</button></div>
                                        <div><button class="btnStyle btnAux" onclick="ejer2Correcta(4)"><b>X + 12</b></button></div>
                                        <div><button class="btnStyle btnAux" onclick="ejer2Incorrecta(4)">18</button></div>
                                        <div><button class="btnStyle btnAux" onclick="ejer2Incorrecta(4)">X + 6</button></div>
                                    </div>
                                </center>
                            </div>
                        </div><br><br>
                        <div class="row" id="ejer2Mod4Success" style="display: none">
                            <div class="alert alert-success">
                                <strong>Ya puedes continuar!</strong>
                            </div>
                        </div>
                        <div id="snackbar2-4"><label class="alert2"></label></div>
                    </div>

                    <!-- modal 5 -->
                    <div style="display: none" id="Ejer2Modal5">
                        <div class="row">
                            <div class="col-sm-7">
                                <img src="../recursos/img/televisor/tel3.png" style="width:600px; height:400px">
                            </div>
                            <div class="col-sm-5">
                                <div>
                                    <h3>Ahora que ya tenemos identificadas las incógnitas, podemos
                                        apoyarnos en la geometría para poder solucionar el
                                        problema que tenemos.
                                    </h3>
                                </div>
                                <div>
                                    <h3> Como se ve la imagen la imagen, tenemos un tríangulo rectángulo, por lo
                                        cual podremos usar el teorema de pitágoras para continuar con
                                        el hallazgo de la ecuación que requerimos.
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <h3>El teorema de Pitágoras dice que: “El valor de la hipotenusa al cuadrado es
                                igual a la suma de los cuadrados de sus catetos” para el caso de nuestro
                                ejercicio, ¿Cuál sería la ecuación que cumpla con el teorema de Pitágoras?
                            </h3>
                        </div><br><br>
                        <div class="row" style="margin-left: 130px;">
                            <div style="margin-right: 25px;"><button class="btnStyle" onclick="ejer2Incorrecta(5)"><img src="../recursos/img/televisor/formulas/modal5/opc1new.png" style="width: 250px"></button></div>
                            <div style="margin-right: 25px;"><button class="btnStyle" onclick="ejer2Incorrecta(5)"><img src="../recursos/img/televisor/formulas/modal5/opc2new.png" style="width: 250px"></button></div>
                            <div><button class="btnStyle" onclick="ejer2Correcta(5)"><img src="../recursos/img/televisor/formulas/modal5/correctanew.png" style="width: 250px;"></button></div>

                        </div>
                        <div class="row" id="ejer2Mod5Success" style="display: none">
                            <div class="alert alert-success">
                                <strong>Ya puedes continuar!</strong>
                            </div>
                        </div>
                        <div id="snackbar2-5"><label class="alert2"></label></div>
                    </div>

                    <!-- modal 6 -->
                    <div style="display: none" id="Ejer2Modal6">

                        <div class="row">
                            <h3>
                                Ahora acabamos de pasar de un problema geométrico a un problema algebraico
                                con la ecuación que encontramos apoyados en el teorema de Pitágoras. Para
                                desarrollar esta ecuación que encontramos vamos a usar la formula del
                                producto notable que dice:
                            </h3>
                        </div><br>
                        <div class="row">
                            <img src="../recursos/img/televisor/formulas/modal6/general.png" style="width: 450px; margin-left: 350px;">
                        </div><br>
                        <div class="row" style="margin-left: 120px;">
                            <h3>Con base en esta fórmula, ¿Cuál crees que es el resultado de <img src="../recursos/img/televisor/formulas/modal6/resDe.png" style="width: 100px;">
                                <h3>
                        </div><br><br>
                        <div class="row">
                            <button class="btnStyle" style="margin-right: 20px; margin-left: 100px" onclick="ejer2Incorrecta(6)"><img src="../recursos/img/televisor/formulas/modal6/opc1new.png" style="width: 200px"></button>
                            <button class="btnStyle" style="margin-right: 20px;" onclick="ejer2Correcta(6)"><img src="../recursos/img/televisor/formulas/modal6/correctanew.png" style="width: 200px"></button>
                            <button class="btnStyle" style="margin-right: 20px;" onclick="ejer2Incorrecta(6)"><img src="../recursos/img/televisor/formulas/modal6/opc2new.png" style="width: 200px"></button>
                            <button class="btnStyle" onclick="ejer2Incorrecta(6)"><img src="../recursos/img/televisor/formulas/modal6/opc3new.png" style="width: 200px"></button>
                        </div>
                        <br><br><br>
                        <div class="row" style="margin-left: 420px;">
                            <span style="font-size: 40px;">(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;) =</span> <img src="../recursos/img/televisor/formulas/modal6/resDe.png" style="width: 150px">
                        </div><br><br>
                        <div class="row" id="ejer2Mod6Success" style="display: none">
                            <div class="alert alert-success">
                                <strong>Ya puedes continuar!</strong>
                            </div>
                        </div>
                        <div id="snackbar2-6"><label class="alert2"></label></div>
                    </div>

                    <!-- modal 7 -->
                    <div style="display: none" id="Ejer2Modal7">
                        <div class="row">
                            <h2 style="text-align: center; margin-top: 80px">
                                Muy bien, ahora usando la misma fórmula, ¿Cuál crees que es el resultado de (X + 6)2?
                            </h2>
                        </div><br><br><br>
                        <div class="row">
                            <button class="btnStyle" style="margin-right: 20px; margin-left: 100px;" onclick="ejer2Correcta(7)"><img src="../recursos/img/televisor/formulas/modal7/correctanew.png" style="width: 200px"></button>
                            <button class="btnStyle" style="margin-right: 20px;" onclick="ejer2Incorrecta(7)"><img src="../recursos/img/televisor/formulas/modal7/opc1new.png" style="width: 200px"></button>
                            <button class="btnStyle" style="margin-right: 20px;" onclick="ejer2Incorrecta(7)"><img src="../recursos/img/televisor/formulas/modal7/opc2new.png" style="width: 200px"></button>
                            <button class="btnStyle" style="margin-right: 20px;" onclick="ejer2Incorrecta(7)"><img src="../recursos/img/televisor/formulas/modal7/opc3new.png" style="width: 200px"></button>
                        </div>
                        <br><br><br><br>
                        <div class="row">
                            <img src="../recursos/img/televisor/formulas/modal7/general.png" style="width: 400px; margin-left: 380px;"> (&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)
                        </div><br><br>
                        <div class="row" id="ejer2Mod7Success" style="display: none">
                            <div class="alert alert-success">
                                <strong>Ya puedes continuar!</strong>
                            </div>
                        </div>
                        <div id="snackbar2-7"><label class="alert2"></label></div>
                    </div>

                    <!-- modal 8 -->
                    <div style="display: none" id="Ejer2Modal8">
                        <div class="row">
                            <h2 style="text-align: center; margin-top: 40px; margin-left: 300px;">
                                Muy bien, la ecuación resultante es:
                            </h2>
                        </div><br>
                        <div class="row">
                            <img src="../recursos/img/televisor/formulas/modal7.7/general1.png" style="width: 450px; margin-left: 350px;">
                        </div><br>
                        <div class="row">
                            <h2 style="text-align: center; ">
                                Como la ecuación resultante es de segundo grado, debemos igualarla
                                a cero, para lo cual tendríamos:
                            </h2>
                        </div><br>
                        <div class="row">
                            <img src="../recursos/img/televisor/formulas/modal7.7/general2.png" style="width: 450px; margin-left: 350px;">
                        </div><br>
                        <div class="row">
                            <h3 style="margin-left: 20px;">Si agrupamos y operamos esta ecuación, ¿Cuál crees que sería la ecuación resultante?</h3>
                        </div><br><br>
                        <div class="row">
                            <button class="btnStyle" style="margin-right: 15px; margin-left: 50px;" onclick="ejer2Incorrecta(8)"><img src="../recursos/img/televisor/formulas/modal7.7/opc1new.png" style="width: 230px;"></button>
                            <button class="btnStyle" style="margin-right: 15px;" onclick="ejer2Incorrecta(8)"><img src="../recursos/img/televisor/formulas/modal7.7/opc2new.png" style="width: 230px;"></button>
                            <button class="btnStyle" style="margin-right: 15px;" onclick="ejer2Correcta(8)"><img src="../recursos/img/televisor/formulas/modal7.7/correctanew.png" style="width: 230px;"></button>
                            <button class="btnStyle" onclick="ejer2Incorrecta(8)"><img src="../recursos/img/televisor/formulas/modal7.7/opc3new.png" style="width: 230px;"></button>
                        </div><br><br>
                        <div class="row" id="ejer2Mod8Success" style="display: none">
                            <div class="alert alert-success">
                                <strong>Ya puedes continuar!</strong>
                            </div>
                        </div>
                        <div id="snackbar2-8"><label class="alert2"></label></div>

                    </div>

                    <!-- modal 9 -->
                    <div style="display: none" id="Ejer2Modal9">
                        <div class="row">
                            <h3 style="text-align: center;">
                                Ya tenemos nuestra ecuación de segundo grado, la cual podemos operar
                                para terminar de resolver el ejercicio planteado. Ahora debes resolver
                                la ecuación <b>-X2 + 12X + 108 = 0</b> la cual es mas practico resolverla con la
                                formula general que es:
                            </h3>
                        </div><br>
                        <div class="row">
                            <img src="../recursos/img/televisor/formulas/modal8/modelo.png" style="height: 70px; margin-left: 450px;">
                        </div><br>
                        <div class="row">
                            <h3 style="margin-left: 60px;">De acuerdo a esta información, ¿Cuál seria la formula aplicada correctamente?</h3>
                        </div><br><br>
                        <div class="row">
                            <div style="margin-left: 100px"><button class="btnStyle" onclick="ejer2Correcta(9)"><img src="../recursos/img/televisor/formulas/modal8/correctanew.png" style="height: 70px; width: 280px;"></button></div>
                            <div style="margin-left: 15px"><button class="btnStyle" onclick="ejer2Incorrecta(9)"><img src="../recursos/img/televisor/formulas/modal8/opc1new.png" style="height: 70px; width: 280px;"></button></div>
                            <div style="margin-left: 15px"><button class="btnStyle" onclick="ejer2Incorrecta(9)"><img src="../recursos/img/televisor/formulas/modal8/opc2new.png" style="height: 70px; width: 280px;"></button></div>
                        </div><br><br>
                        <div class="row" id="ejer2Mod9Success" style="display: none">
                            <div class="alert alert-success">
                                <strong>Ya puedes continuar!</strong>
                            </div>
                        </div>
                        <div id="snackbar2-9"><label class="alert2"></label></div>
                    </div>

                    <!-- modal 10 -->
                    <div style="display: none" id="Ejer2Modal10">
                        <div class="row">
                            <h2 style="text-align: center; margin-top: 50px">
                                Correcto, aplicaste muy bien la formula general para resolver ecuaciones de
                                segundo grado. Las dos soluciones que nos da la ecuación serían las
                                siguientes:
                            </h2>
                        </div><br><br><br>
                        <div class="row">
                            <div class="col-sm-4">
                                <button class="btnStyle" style="margin-bottom: 20px;" onclick="ejer2Incorrecta(10)"><img src="../recursos/img/televisor/formulas/modal9/opc1new.png" style="height: 70px; width: 280px;"></button>
                                <button class="btnStyle" onclick="ejer2Correcta(10)"><img src="../recursos/img/televisor/formulas/modal9/correctanew.png" style="height: 70px; width: 280px;"></button>
                            </div>
                            <div class="col-sm-8">
                                <h3>¿Cuál de los dos resultados crees que es el correcto?</h3>
                            </div>
                        </div><br><br>
                        <div class="row" id="ejer2Mod10Success" style="display: none">
                            <div class="alert alert-success">
                                <strong>Ya puedes continuar!</strong>
                            </div>
                        </div>
                        <div id="snackbar2-10"><label class="alert2"></label></div>
                    </div>

                    <!-- modal final -->
                    <div style="display: none" id="Ejer2Modal11">
                        <div class="row">
                            <h3>Muy bien, el valor correcto es 18, ya que el primer valor nos daba
                                negativo (-6) y como estamos hallando una longitud, esta no podría
                                ser negativa. Ahora con este valor podemos hallar el largo. Ancho y
                                la diagonal de nuestro televisor, las cuales serían:
                            </h3>
                        </div><br>
                        <div class="row">
                            <div class="col-sm-7">
                                <img src="../recursos/img/televisor/tel5.png" style="width:600px; height:400px">
                            </div>
                            <div class="col-sm-5" style="font-size: 25px;;">
                                <div><b>Alto => X = 18<b></div>
                                <div><b>Ancho => X + 6 = 18 + 6 = 24<b></div>
                                <div><b>Diagonal => X + 12 = 18 + 12 = 30<b></div><br><br>
                                <button class="btn btn-success" id="finalizarEjer2" style="width: 3cm; height: 1.5cm; font-size: 20px;">Finalizar</button>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="alert alert-success">
                                <strong>Haz terminado el ejercicio, buen trabajo!</strong>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer" id="Ejer1ModalEnc">
                    <a><img src="../recursos/img/volver.jpg" style="width:80px; height:59px; margin-right: 920px" onclick="volverEjer2()" id="volverEjer2"></a>
                    <button class="btn btn-primary" id="continuarEjer2">Continuar</button>
                </div>
            </div>
        </div>
    </div>


    <footer class="w3-container w3-center w3-opacity">
        <div class="w3-xlarge w3-padding-32">
            <a class="fa fa-facebook-official w3-hover-opacity" href="https://es-la.facebook.com/UniremingtonOficial/" target="_blank"></a>
            <a class="fa fa-instagram w3-hover-opacity" href="https://www.instagram.com/uniremington_medellin/" target="_blank"></a>
            <a class="fa fa-twitter w3-hover-opacity" href="https://twitter.com/Uni_Remington?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor" target="_blank"></a>
            <a class="fa fa-linkedin w3-hover-opacity" href="https://co.linkedin.com/company/uniremington" target="_blank"></a>
        </div>
        <div class="row">
            <div class="col-xs-12 col-ms-9 col-md-12 col-lg-12" id="grad">
                <center>
                    <h5 style="font-weight: bold;">www.uniremington.edu.co <br>
                        Medellín Antioquia, Colombia<br>
                        &copy;<br>
                        Acosta A., Castro A. & Mora N.</h5>
                </center>
            </div>
        </div>
    </footer>


    <script src="../recursos/js/script.js"></script>
    <script src="../recursos/jsLibrerias/popper.min.js"></script>
    <script src="../recursos/jsLibrerias/bootstrap.min.js"></script>
    <script src="../recursos/jsLibrerias/jquery-3.3.1.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="../recursos/jquery-ui-1.12.1/jquery-ui.js"></script>
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"> </script>
    <script src="https://www.geogebra.org/apps/deployggb.js"></script>
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"> </script>



    <script>
        let contEjer1 = 1;
        let contEjer2 = 1;
        let percent1 = 0;
        let percent2 = 0;
        let pts = 0;
        let pts2 = 0;
        let acuTotal = 0;
        let finEjer1 = 0;
        let finEjer2 = 0;

        $('#finalizarEjer1').click(e => {
            acuTotal += pts;
            $(".btnEjerPisc").prop('disabled', true);
            $('#modalEjer1').modal('hide');
            finEjer1 = 1;
        });

        $('#finalizarEjer2').click(e => {
            acuTotal += pts2;
            $(".btnEjerTel").prop('disabled', true);
            $('#modalEjer2').modal('hide');
            finEjer2 = 1;
        });

        $('#juegoFinal').click(e => {
            debugger;
            let sw = 1;
            if (finEjer1 == 0 && finEjer2 == 0) {
                $('#alert').text('Se requiere realiazar los ejercicios anteriores');
                $(`#snackbar`).css('margin-left', '-300px');
                sw = 0;
            } else if (finEjer1 == 0) {
                $(`#snackbar`).css('margin-left', '-255px');
                $('#alert').text('Falta realizar el ejercicio de la Piscina');
                sw = 0;
            } else if (finEjer2 == 0) {
                $(`#snackbar`).css('margin-left', '-300px');
                $('#alert').text('Falta realizar el ejercicio del Televisor');
                sw = 0;
            }
            $(`#snackbar`).css('background-color', '#fff');
            var x = document.getElementById("snackbar");
            x.className = "show";

            setTimeout(function() {
                x.className = x.className.replace("show", "");
            }, 2000);


            if (sw == 1) {
                $(location).attr('href', `Juego.php?acu12=${acuTotal}&id=<?php print $id; ?>&nom=<?php print $nom; ?>&ape=<?php print $ape; ?>`);
            }
        });

        $('#btn').click(e => {
            start();
            $('#modalEjer1').modal('show');

            $('.mod4correcta, .mod4opc1, .mod4opc2').draggable();

            $("#res").droppable({
                accept: ".mod4correcta",
                drop: function(event, ui) {
                    $(this).css('background', '#B4DEF8');
                    $('#continuarEjer1').css('display', 'block');
                    $('#volverEjer1').css('margin-right', '920px');
                    $('#ejer1Mod4Success').css('display', 'block');

                },
                out: function(event, ui) {
                    $(this).css('background', '#E6E6E6');
                }
            });
        });

        $('#btn4').click(e => {
            $('#modalEjer4').modal('show');
        });

        $("#modalEjer1").on('hidden.bs.modal', function() {
            stop();
            reset();
        });

        $('#btn2').click(e => {
            $('#modalEjer2').modal('show');

            $('#volverEjer2').css('display', 'none');
        });

        function ejer1Correcta(mod) {

            $('#continuarEjer1').css('display', 'block');
            $('#volverEjer1').css('margin-right', '910px');
            $(`#snackbar1-${mod}`).css('margin-left', '-125px');
            $(`#snackbar1-${mod}`).css('background-color', '#2DC44D');
            $('.alert2').text('Correcto, Puedes Continuar!! +5 Pts');
            var x = document.getElementById(`snackbar1-${mod}`);
            x.className = "show";

            setTimeout(function() {
                x.className = x.className.replace("show", "");
            }, 2000);
        }

        function ejer1Incorrecta(mod) {
            debugger;
            pts -= 5;
            $('#pts').val(pts);
            $(`#snackbar1-${mod}`).css('margin-left', '-125px');
            $(`#snackbar1-${mod}`).css('background-color', '#EA1212');
            $('.alert2').text('Opción Incorrecta -5 Pts');
            var x = document.getElementById(`snackbar1-${mod}`);
            x.className = "show";

            setTimeout(function() {
                x.className = x.className.replace("show", "");
            }, 2000);
        }

        $('#ejer1Mod11ecu2').click(e => {
            $('#continuarEjer1').css('display', 'none');
            $('#volverEjer1').css('margin-right', '1020px');
            $('#ejer1Mod11Success').css('display', 'block')
            stop();
            //percent1 += 9;
            $('.progress-bar').css('width', 100 + '%');
            $('.progress-bar').attr('aria-valuenow', 100);
            $('.progress-bar').text(100 + '%');
            pts += 5;
            $('#pts').val(pts);
        });

        $('#continuarEjer1').click(e => {
            $('#continuarEjer1').css('display', 'block');
            $('#volverEjer1').css('display', 'block');
            contEjer1++;
            console.log(contEjer1);

            if (contEjer1 == 4) {
                $('#continuarEjer1').css('display', 'none');
                $('#volverEjer1').css('margin-right', '1020px');
            }

            if (contEjer1 == 5) {
                $('#continuarEjer1').css('display', 'none');
                $('#volverEjer1').css('margin-right', '1020px');
            }

            if (contEjer1 == 7) {
                $('#continuarEjer1').css('display', 'none');
                $('#volverEjer1').css('margin-right', '1020px');
            }

            if (contEjer1 == 8) {
                $('#continuarEjer1').css('display', 'none');
                $('#volverEjer1').css('margin-right', '1020px');
            }

            if (contEjer1 == 9) {
                $('#continuarEjer1').css('display', 'none');
                $('#volverEjer1').css('margin-right', '1020px');
            }

            if (contEjer1 == 10) {
                $('#continuarEjer1').css('display', 'none');
                $('#volverEjer1').css('margin-right', '1020px');
            }

            if (contEjer1 == 11) {
                $('#continuarEjer1').css('display', 'none');
                $('#volverEjer1').css('display', 'none');
            }
            $(`#Ejer1Modal${contEjer1 -1}`).css('display', 'none');
            $(`#Ejer1Modal${contEjer1}`).css('display', 'block');

            percent1 += 9;
            $('#progress1').css('width', percent1 + '%');
            $('#progress1').attr('aria-valuenow', percent1);
            $('#progress1').text(percent1 + '%');

            pts += 5;
            $('#pts').val(pts);

        });

        function volverEjer1() {
            contEjer1--;
            if (contEjer1 == 1) {
                $('#volverEjer1').css('display', 'none');
            }
            $('#volverEjer1').css('margin-right', '920px');
            $(`#Ejer1Modal${contEjer1}`).css('display', 'block');
            $(`#Ejer1Modal${contEjer1 + 1}`).css('display', 'none');
            $('#continuarEjer1').css('display', 'block');

            percent1 -= 9;
            $('.progress-bar').css('width', percent1 + '%');
            $('.progress-bar').attr('aria-valuenow', percent1);
            $('.progress-bar').text(percent1 + '%');
        };

        $('#continuarEjer2').click(e => {
            debugger;
            $('#volverEjer2').css('display', 'block');
            contEjer2++;
            if (contEjer2 == 11) {
                $('#continuarEjer2').css('display', 'none');
                $('#volverEjer2').css('margin-right', '1010px');
            }

            if (contEjer2 == 2) {
                $('#continuarEjer2').css('display', 'none');
                $('#volverEjer2').css('margin-right', '1010px');
            }

            if (contEjer2 == 3) {
                $('#continuarEjer2').css('display', 'none');
                $('#volverEjer2').css('margin-right', '1010px');
            }

            if (contEjer2 == 4) {
                $('#continuarEjer2').css('display', 'none');
                $('#volverEjer2').css('margin-right', '1010px');
            }

            if (contEjer2 == 5) {
                $('#continuarEjer2').css('display', 'none');
                $('#volverEjer2').css('margin-right', '1010px');
            }

            if (contEjer2 == 6) {
                $('#continuarEjer2').css('display', 'none');
                $('#volverEjer2').css('margin-right', '1010px');
            }

            if (contEjer2 == 7) {
                $('#continuarEjer2').css('display', 'none');
                $('#volverEjer2').css('margin-right', '1010px');
            }

            if (contEjer2 == 8) {
                $('#continuarEjer2').css('display', 'none');
                $('#volverEjer2').css('display', 'none');
            }
            if (contEjer2 == 9) {
                $('#continuarEjer2').css('display', 'none');
                $('#volverEjer2').css('margin-right', '1010px');
            }
            if (contEjer2 == 10) {
                $('#continuarEjer2').css('display', 'none');
                $('#volverEjer2').css('margin-right', '1010px');
            }


            $(`#Ejer2Modal${contEjer2 -1}`).css('display', 'none');
            $(`#Ejer2Modal${contEjer2}`).css('display', 'block');

            percent2 += 10;
            $('#progress2').css('width', percent2 + '%');
            $('#progress2').attr('aria-valuenow', percent2);
            $('#progress2').text(percent2 + '%');

            pts2 += 5;
            $('#pts2').val(pts2);
        });

        function ejer2Correcta(mod) {
            $('#continuarEjer2').css('display', 'block');
            $('#volverEjer2').css('margin-right', '910px');
            $(`#snackbar2-${mod}`).css('margin-left', '-125px');
            $(`#snackbar2-${mod}`).css('background-color', '#2DC44D');
            $('.alert2').text('Correcto, Puedes Continuar!! +5 Pts');
            var x = document.getElementById(`snackbar2-${mod}`);
            x.className = "show";

            setTimeout(function() {
                x.className = x.className.replace("show", "");
            }, 2000);
        }

        function ejer2Incorrecta(mod) {
            pts2 -= 5;
            $('#pts2').val(pts2);
            $(`#snackbar2-${mod}`).css('margin-left', '-125px');
            $(`#snackbar2-${mod}`).css('background-color', '#EA1212');
            $('.alert2').text('Opción Incorrecta -5 Pts');
            var x = document.getElementById(`snackbar2-${mod}`);
            x.className = "show";

            setTimeout(function() {
                x.className = x.className.replace("show", "");
            }, 2000);
        }

        function volverEjer2() {
            contEjer2--;
            if (contEjer2 == 1) {
                $('#volverEjer2').css('display', 'none');
            }
            $('#volverEjer2').css('margin-right', '920px');
            $(`#Ejer2Modal${contEjer2}`).css('display', 'block');
            $(`#Ejer2Modal${contEjer2 + 1}`).css('display', 'none');
            $('#continuarEjer2').css('display', 'block');

            percent2 -= 9;
            $('.progress-bar').css('width', percent2 + '%');
            $('.progress-bar').attr('aria-valuenow', percent2);
            $('.progress-bar').text(percent2 + '%');

            pts2 -= 5;
            $('#pts2').val(pts2);
        };

        window.onload = function() {
            pantalla = document.getElementById("screen");
        }

        window.onload = function() {
            pantalla = document.getElementById("screen");
        }

        var isMarch = false;
        var acumularTime = 0;

        function cronometro() {
            timeActual = new Date();
            acumularTime = timeActual - timeInicial;
            acumularTime2 = new Date();
            acumularTime2.setTime(acumularTime);
            cc = Math.round(acumularTime2.getMilliseconds() / 10);
            ss = acumularTime2.getSeconds();
            mm = acumularTime2.getMinutes();
            hh = acumularTime2.getHours() - 18;
            if (cc < 10) {
                cc = "0" + cc;
            }
            if (ss < 10) {
                ss = "0" + ss;
            }
            if (mm < 10) {
                mm = "0" + mm;
            }
            if (hh < 10) {
                hh = "0" + hh;
            }
            pantalla.innerHTML = mm + " : " + ss + " : " + cc;
        }

        function start() {
            if (isMarch == false) {
                timeInicial = new Date();
                control = setInterval(cronometro, 10);
                isMarch = true;
            }
        }

        function stop() {
            if (isMarch == true) {
                clearInterval(control);
                isMarch = false;
            }
        }

        function resume() {
            if (isMarch == false) {
                timeActu2 = new Date();
                timeActu2 = timeActu2.getTime();
                acumularResume = timeActu2 - acumularTime;

                timeInicial.setTime(acumularResume);
                control = setInterval(cronometro, 10);
                isMarch = true;
            }
        }

        function reset() {
            if (isMarch == true) {
                clearInterval(control);
                isMarch = false;
            }
            acumularTime = 0;
            pantalla.innerHTML = "00 : 00 : 00";
        }
    </script>
</body>

</html>