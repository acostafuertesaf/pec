<?php

require_once '../control/UsuarioControl.php';
$usuarios = [];
$usuarioControl = new UsuarioControl();
$ranking = 0;

$acu12 = $_GET['acu12'];
$id = $_GET['id'];
$nom = $_GET['nom'];
$ape = $_GET['ape'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../recursos/cssLibrerias/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="icon" type="image/png" href="../recursos/img/pec5.png" />
    <title> <?php print $nom; ?> estas jugando quien quiere ser Matemático</title>
    <style>
        footer {
            width: 100%;
            position: absolute;
            bottom: 0;
            color: #000;
        }

        html {
            min-height: 100%;
            position: relative;
        }

        .btn[id='btnJugar'],
        [id='btninstrucciones'],
        [id='btnRanking'] {
            padding: 3px 10px;
            border: black 3px solid;
            border-top-left-radius: 100px;
            border-bottom-right-radius: 100px;
            background-color: #2e75b6;
            color: white;
            -webkit-transition: all .5s ease;
            -moz-transition: all .5s ease;
            -o-transition: all .5s ease;
            -ms-transition: all .5s ease;
        }

        .btn {
            padding: 3px 10px;
            border: black 3px solid;
            border-top-left-radius: 100px;
            border-bottom-right-radius: 100px;
            background-color: #2e75b6;
            color: white;
            width: 30%;
        }

        #btnJugar:hover,
        #btninstrucciones:hover,
        #btnRanking:hover {
            transform: scale(1.1);
            background: #B0DEFF;
            color: black;
        }

        #btnResA:hover,
        #btnResB:hover,
        #btnResC:hover,
        #btnResD:hover {
            background: #B0DEFF;
            color: black;
        }

        body {
            /* background: #8B8B8B; */
            background: linear-gradient(30deg, #165DEB, #4078E8, #2e75b6, #B0DEFF, white, #B0DEFF, #4078E8, #2e75b6, #165DEB);
            background-size: cover;
            background-repeat: no-repeat;
            background-attachment: fixed;
        }

        section {
            float: left;
            /* background: linear-gradient(white, #B0DEFF, #2e75b6 ); */
            /* background: linear-gradient(#000); */
            background: #eee;
            /*border-radius: 15px;*/
            border-radius: 50px;
            /* background: linear-gradient(#2e75b6, #B0DEFF, white ); */
            border: black 3px solid;
            margin-top: 2cm;
            height: 750px;
        }

        aside {
            float: right;
            width: 20%;
            background: #eee;
            height: 750px;
            margin-top: 2cm;
            border: black 3px solid;
            border-radius: 50px;
        }

        .preguntas {
            display: none;
            font-size: 20px;
        }

        .A,
        .B,
        .C,
        .D {
            display: none;
            font-size: 18px;
            padding: 5px;
        }

        .circulo {
            width: 30px;
            height: 30px;
            -moz-border-radius: 50%;
            -webkit-border-radius: 50%;
            border-radius: 50%;
            background: red;
            margin-top: 15px;
            margin-left: 0.5cm;
            /* #5cb85c */
        }

        .acu {
            font-weight: bold;
            margin-left: 3px;
            color: black;
        }

        .btnInicio {
            font-weight: bold;
            font-size: 22px;

        }

        .panelPre {
            height: 100px;
            width: 600px;
            background: #fff;
            margin-bottom: 15px;
            border-radius: 25px;
            border: 2px black solid;
            margin-top: -15px;

        }

        .fontPts {
            font-style: normal;
            font-weight: bold;
        }

        .return {
            color: white;
        }

        .return:hover {
            color: black;
        }

        .titleRes {
            font-weight: bold;
            font-family: 'times new roman';
            font-size: 20px;
        }

        ol {
            font-size: 20px;
            font-family: 'arial';
        }

        #snackbar {
            visibility: hidden;
            min-width: 400px;
            margin-left: -125px;
            margin-bottom: 45%;
            color: #000;
            background-color: red;
            font-weight: bold;
            text-align: center;
            border-radius: 5px;
            padding: 16px;
            position: fixed;
            z-index: 1;
            left: 50%;
            bottom: 30px;
            font-size: 25px;
        }

        #snackbar.show {
            visibility: visible;
            -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
            animation: fadein 0.5s, fadeout 0.5s 2.5s;
        }



        @-webkit-keyframes fadein {
            from {
                bottom: 0;
                opacity: 0;
            }

            to {
                bottom: 30px;
                opacity: 1;
            }
        }

        @keyframes fadein {
            from {
                bottom: 0;
                opacity: 0;
            }

            to {
                bottom: 30px;
                opacity: 1;
            }
        }

        @-webkit-keyframes fadeout {
            from {
                bottom: 30px;
                opacity: 1;
            }

            to {
                bottom: 0;
                opacity: 0;
            }
        }

        @keyframes fadeout {
            from {
                bottom: 30px;
                opacity: 1;
            }

            to {
                bottom: 0;
                opacity: 0;
            }
        }
    </style>
</head>
<script src="../recursos/js/script.js"></script>

<body>
    <header class="w3-top" style="height: 2cm;">
        <span id="screen1" style="font-size: 45px; color: black; font-weight: bold; display: none" class="col-md-4 w3-left">00 : 00</span>
        <span class="w3-right w3-padding " style="font-size: 20px; color: white; font-weight: bold;">Profesor de Ecuaciones Cuadráticas</span>
        <a id="regresarHome" href="home.php?nom=<?php print $nom ?>&ape=<?php print $ape ?>&id=<?php print $id ?>" class="w3-right w3-padding return" style="font-size: 20px; 
        font-weight: bold; text-decoration: none;"><span><img src="../recursos/img/regresar.png" style="width: 25px;" />Home</span></a>
        <div class="row" style="margin-left: 1cm">
        </div>
    </header>

    <tbody>
        <!-- Home -->
        <div class="container initJuego" style="">
            <!-- <div class="spinner-border text-warning" style="width: 100px; height: 100px;"></div> -->
            <section style="width: 100%;">
                <div class="row">
                    <div class="col-md-4"></div>
                    <img class="col-md-4" src="../recursos/img/pecc6.png" style="width: 380px; height: 300px;">
                </div>
                <div class="row" style="margin-top: 10%; ">
                    <div class="col-md-1"></div>
                    <button class="col-md-3 btn " id="btnJugar" style="height: 2cm; margin-right: 1.5cm;"><span class="btnInicio">JUGAR</span></button>

                    <button class="col-md-3 btn " style="height: 2cm; margin-right: 1.5cm;" data-toggle="modal" data-target="#modalInstrucciones" id="btninstrucciones"><span class="btnInicio">INSTRUCCIONES<span></button>

                    <button class="col-md-3 btn " style="height: 2cm; " data-toggle="modal" data-target="#modalRanking" id="btnRanking"><span class="btnInicio">RANKING<span></button>
                    <div class="col-md-1"></div> </button>
                </div>
            </section>
        </div>
        <!-- Juego -->
        <div class="container" style="display: none;" id="juegoFinal">
            <section style="width: 80%;">
                <div class="row">
                    <div class="col-md-4 w3-center" style="font-size: 12px; font-weight: bold;"><span><img src="../recursos/img/cronometro.png" style="width: 20px;" /> </span>Cronometro por Pregunta </div>
                    <div class="col-md-4"></div>
                    <div class="col-md-4 w3-center" style="font-size: 12px; font-weight: bold;">Comodines</div>
                </div>
                <div class="row">
                    <div id="screen2" style="font-size: 25px; color: black; font-weight: bold;" class="col-md-4 w3-center">00 : 00</div>
                    <img class="col-md-4" src="../recursos/img/pecc6.png" style="width: 280px; height: 260px;">
                    <div class="col-md-4 w3-center">
                        <a href="#"><img id="btnMedio" src="../recursos/img/final/medio1.png" style="width: 40px; height: 40px;"></a>
                        <a href="#"><img id="btnWapp" src="../recursos/img/final/wapp1.png" style="width: 40px; height: 40px;"></a>
                        <!-- <a href="#"><img id="btnMayoria" src="../recursos/img/final/mayoria2.png" style="width: 40px; height: 40px;"></a> -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4 w3-center" style="margin-top: -10px"><label id="acumuladoo" style="color: black; font-size: 45px;"></label><span style="margin-left: 5px; font-style: italic;"><label id="acuPts" style="color: black;">pts</label></span></div>
                </div><br>
                <div>
                    <div class="row">
                        <div class="col-md-1"> </div>
                        <div class="col-md-10 panelPre">
                            <!-- pregunta -->
                            <center><label>
                                    <br>
                                    <div class="preguntas pre1" style="display: block">En la siguiente ecuación cuadrática 4 + 3X + 2X2 = 0 ¿cual sería el correcto valor de los coeficientes a, b y c?</div>
                                    <div class="preguntas pre2" style="display: none">¿Cuál sería el valor del coeficiente C de la siguiente ecuación cuadrática completa: 5X2 + 10X = -20</div>
                                    <div class="preguntas pre3" style="display: none">¿Cuál es la fórmula general para la solución de ecuaciones cuadráticas?</div>
                                    <div class="preguntas pre4" style="display: none"> ¿Cuál de las siguientes afirmaciones es la correcta si sabemos que en la formula general el término [b^2-4(a)(c)]=0</div>
                                    <div class="preguntas pre5" style="display: none">¿Cuáles serían los valores de X si resolvemos la ecuación 3x2 - 5x + 2 = 0?</div>
                                    <div class="preguntas pre6" style="display: none">Si resolvemos la ecuación X2 + X – 6 = 0 factorizando, ¿Cuál sería el resultado correcto?</div>
                                    <div class="preguntas pre7" style="display: none">Si queremos normalizar la ecuación X2 + (x-2)2 = 130 ¿Cuál sería la ecuación cuadratica perfecta resultante?</div>
                                    <div class="preguntas pre8" style="display: none">Si despues de factorizar una ecución el resultados es (X - 2)(X + 9) = 0, ¿Cuál sería la ecuación cuadratica completa?</div>
                                    <div class="preguntas pre9" style="display: none">¿Que tipo de ecuación cuadrática tendremos si los coeficientes a,b,c son diferentes de 0?</div>
                                    <div class="preguntas pre10" style="display: none">¿Cuál de las siguientes ecuaciones NO es una ecuación cuadrática incompleta?</div>
                                    <div class="preguntas pre11" style="display: none">¿Cuál de las siguientes gráficas corresponde a una función cuadrática?</div>
                                    <div class="preguntas pre12" style="display: none">¿Cuál de las siguientes afirmaciones es la correcta si sabemos que en la formula general el término [b^2-4(a)(c)]>0</div>
                                    <div class="preguntas pre13" style="display: none">¿Cuáles serían los valores de X si resolvemos la ecuación 8X + 5 = 36X^2?</div>
                                    <div class="preguntas pre14" style="display: none">¿Cuál es la ecuación cuadrática resultante si normalizamos la expresión (5X - 4)^2 - (3X + 5)(2X - 1) = 20X(X - 2) + 27</div>
                                    <div class="preguntas pre15" style="display: none">¿Cual es la ecuación cuadrática normalizada y los valores de X de la expresión (1000/(X + 10)) = (1000/X) - 5?</div>
                                </label></center>
                        </div>
                    </div>
                    <div>
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-5 w3-center titleRes">A</div>
                            <div class="col-md-5 w3-center titleRes">B</div>
                        </div>
                        <!-- Primera fila de respuestas -->
                        <div class="row" style="padding-bottom: 1cm;">
                            <div class="col-md-1"></div>
                            <!-- Respuestas A -->
                            <button style=" height: 2cm;" class="col-md-5 btn" id="btnResA">
                                <div class="preguntas res1 A" id="resA1" onclick="resIncorrecta(1)" style="display: block" data-value="0">a = 4; b = 3; c = 0</div>
                                <div class="preguntas res2 A" id="resA2" onclick="resCorrecta(2)" data-value="1">20</div>
                                <div style="background-color: white; width: 70%; margin-left:15%" class="preguntas res3 A" id="resA3" onclick="resIncorrecta(3)" data-value="0"><img src="../recursos/img/fGeneral11.png"/></div>
                                <div class="preguntas res4 A" id="resA4" onclick="resIncorrecta(4)" data-value="0">Hay dos raíces con número reales.</div>
                                <div class="preguntas res5 A" id="resA5" onclick="resCorrecta(5)" data-value="1">X1 = 1 ; X2 = 2/3</div>
                                <div class="preguntas res6 A" id="resA6" onclick="resIncorrecta(6)" data-value="0">(X + 6)(X - 1) = 0</div>
                                <div class="preguntas res7 A" id="resA7" onclick="resIncorrecta(7)" data-value="0">X2 - 2X + 130 = 0</div>
                                <div class="preguntas res8 A" id="resA8" onclick="resIncorrecta(8)" data-value="0">X2 + 18X + 7 = 0</div>
                                <div class="preguntas res9 A" id="resA9" onclick="resIncorrecta(9)" data-value="0">Ecuación cuadrática incompleta</div>
                                <div class="preguntas res10 A" id="resA10" onclick="resIncorrecta(10)" data-value="0">aX2 + bX = 0</div>
                                <div class="preguntas res11 A" id="resA11" onclick="resCorrecta(11)" data-value="1"><img src="../recursos/img/Pregunta_11/Cuadratica.png" style="width:180px; height:65px"></div>
                                <div class="preguntas res12 A" id="resA12" onclick="resIncorrecta(12)" data-value="0">Las dos raíces son negativas.</div>
                                <div class="preguntas res13 A" id="resA13" onclick="resCorrecta(13)" data-value="1">X1 = 1/2 ; X2 = -5/18</div>
                                <div class="preguntas res14 A" id="resA14" onclick="resIncorrecta(14)" data-value="0">-X^2 + 7X - 6 = 0</div>
                                <div class="preguntas res15 A" id="resA15" onclick="resIncorrecta(15)" data-value="0">ecuacuón: -5X^2 -50X -10000; Raices: X1 = 50 ; X2 = -40</div>
                            </button>

                            <!-- Respuestas B -->
                            <button style="height: 2cm;" class="col-md-5 btn" id="btnResB">
                                <div class="preguntas res1 B" id="resB1" onclick="resIncorrecta(1)" style="display: block" data-value="0">a = 3; b = 2; c = 4</div>
                                <div class="preguntas res2 B" id="resB2" onclick="resIncorrecta(2)" data-value="0">10</div>
                                <div style="background-color: white; width: 70%; margin-left:15%" class="preguntas res3 B" id="resB3" onclick="resIncorrecta(3)" data-value="0"><img src="../recursos/img/fGeneral22.png"/></div>
                                <div class="preguntas res4 B" id="resB4" onclick="resCorrecta(4)" data-value="1">Solo hay una raíz para la ecuación.</div>
                                <div class="preguntas res5 B" id="resB5" onclick="resIncorrecta(5)" data-value="0">X1 = 1 ; X2 = 5</div>
                                <div class="preguntas res6 B" id="resB6" onclick="resCorrecta(6)" data-value="1">(X + 3)(X - 2) = 0</div>
                                <div class="preguntas res7 B" id="resB7" onclick="resIncorrecta(7)" data-value="0">X2 + 2X - 63 = 0</div>
                                <div class="preguntas res8 B" id="resB8" onclick="resIncorrecta(8)" data-value="0">X2 + 7X + 18 = 0</div>
                                <div class="preguntas res9 B" id="resB9" onclick="resIncorrecta(9)" data-value="0">Ecuación con soluciones complejas</div>
                                <div class="preguntas res10 B" id="resB10" onclick="resCorrecta(10)" data-value="1">bX + c = 0</div>
                                <div class="preguntas res11 B" id="resB11" onclick="resIncorrecta(11)" data-value="0"><img src="../recursos/img/Pregunta_11/lineal.png" style="width:180px; height:65px"></div>
                                <div class="preguntas res12 B" id="resB12" onclick="resIncorrecta(12)" data-value="0">No hay una solución real.</div>
                                <div class="preguntas res13 B" id="resB13" onclick="resIncorrecta(13)" data-value="0">X1 = 1/2 ; X2 = -5/36</div>
                                <div class="preguntas res14 B" id="resB14" onclick="resIncorrecta(14)" data-value="0">X^2 - 7X + 6 = 0</div>
                                <div class="preguntas res15 B" id="resB15" onclick="resCorrecta(15)" data-value="1">ecuacuón: 5X^2 + 50X - 10000; Raices: X1 = 40 ; X2 = -50</div>
                            </button>
                        </div>
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-5 w3-center titleRes">C</div>
                            <div class="col-md-5 w3-center titleRes">D</div>
                        </div>
                        <!-- Segunda fila de respuestas -->
                        <div class="row">
                            <div class="col-md-1"></div>
                            <!-- Respuestas C -->
                            <button style=" height: 2cm; width: 80%;" class="col-md-5 btn" id="btnResC">
                                <div class="preguntas res1 C" id="resC1" onclick="resCorrecta(1)" style="display: block" data-value="1">a = 2; b = 3; c = 4</div>
                                <div class="preguntas res2 C" id="resC2" onclick="resIncorrecta(2)" data-value="0">-20</div>
                                <div style="background-color: white; width: 70%; margin-left:15%" class="preguntas res3 C" id="resC3" onclick="resIncorrecta(3)" data-value="0"><img src="../recursos/img/fGeneral3.png"/></div>
                                <div class="preguntas res4 C" id="resC4" onclick="resIncorrecta(4)" data-value="0">Las dos raíces son negativas.</div>
                                <div class="preguntas res5 C" id="resC5" onclick="resIncorrecta(5)" data-value="0">X1 = 3/2 ; X2 = 2</div>
                                <div class="preguntas res6 C" id="resC6" onclick="resIncorrecta(6)" data-value="0">(X - 3)(X - 2) = 0</div>
                                <div class="preguntas res7 C" id="resC7" onclick="resCorrecta(7)" data-value="1">X2 - 2X - 63 = 0</div>
                                <div class="preguntas res8 C" id="resC8" onclick="resIncorrecta(8)" data-value="0">X2 - 18X - 7 = 0</div>
                                <div class="preguntas res9 C" id="resC9" onclick="resIncorrecta(9)" data-value="0">Se convertiría en una ecuación de primer grado</div>
                                <div class="preguntas res10 C" id="resC10" onclick="resIncorrecta(10)" data-value="0">aX2 + c = 0</div>
                                <div class="preguntas res11 C" id="resC11" onclick="resIncorrecta(11)" data-value="0"><img src="../recursos/img/Pregunta_11/cubica.png" style="width:180px; height:65px"></div>
                                <div class="preguntas res12 C" id="resC12" onclick="resCorrecta(12)" data-value="1">Hay dos raíces con número reales.</div>
                                <div class="preguntas res13 C" id="resC13" onclick="resIncorrecta(13)" data-value="0">X1 = 3/2 ; X2 = -1/18</div>
                                <div class="preguntas res14 C" id="resC14" onclick="resIncorrecta(14)" data-value="0">X^2 + 7X + 6 = 0</div>
                                <div class="preguntas res15 C" id="resC15" onclick="resIncorrecta(15)" data-value="0">ecuacuón: 5X^2 -50X + 10000; Raices: X1 = 50 ; X2 = -40</div>
                            </button>

                            <!-- Respuestas D -->
                            <button style=" height: 2cm; width: 80%;" class="col-md-5 btn" id="btnResD">
                                <div class="preguntas res1 D" id="resD1" style="display: block" onclick="resIncorrecta(1)" data-value="0">a = 4; b = 3; c = 2</div>
                                <div class="preguntas res2 D" id="resD2" onclick="resIncorrecta(2)" data-value="0">5</div>
                                <div style="background-color: white; width: 70%; margin-left:15%" class="preguntas res3 D" id="resD3" onclick="resCorrecta(3)" data-value="1"><img src="../recursos/img/fGeneral.png"/></div>
                                <div class="preguntas res4 D" id="resD4" onclick="resIncorrecta(4)" data-value="0">No hay una solución real.</div>
                                <div class="preguntas res5 D" id="resD5" onclick="resIncorrecta(5)" data-value="0">X1 = 2 ; X2 = 5/3</div>
                                <div class="preguntas res6 D" id="resD6" onclick="resIncorrecta(6)" data-value="0">(X + 3)(X + 2) = 0</div>
                                <div class="preguntas res7 D" id="resD7" onclick="resIncorrecta(7)" data-value="0">X2 - 2X + 63 = 0</div>
                                <div class="preguntas res8 D" id="resD8" onclick="resCorrecta(8)" data-value="1">X2 + 7X - 18 = 0</div>
                                <div class="preguntas res9 D" id="resD9" onclick="resCorrecta(9)" data-value="1">Ecuación cuadrática completa</div>
                                <div class="preguntas res10 D" id="resD10" onclick="resIncorrecta(10)" data-value="0">aX2 = 0</div>
                                <div class="preguntas res11 D" id="resD11" onclick="resIncorrecta(11)" data-value="0"><img src="../recursos/img/Pregunta_11/raiz_cuadrada.png" style="width:180px; height:65px"></div>
                                <div class="preguntas res12 D" id="resD12" onclick="resIncorrecta(12)" data-value="0">Solo hay una raíz para la ecuación.</div>
                                <div class="preguntas res13 D" id="resD13" onclick="resIncorrecta(13)" data-value="0">X1 = 1/18 ; X2 = -2/9</div>
                                <div class="preguntas res14 D" id="resD14" onclick="resCorrecta(14)" data-value="1">-X^2 - 7X - 6 = 0</div>
                                <div class="preguntas res15 D" id="resD15" onclick="resIncorrecta(15)" data-value="0">ecuacuón: 5X^2 + 50X + 10000; Raices: X1 = 40 ; X2 = -50</div>
                            </button>
                        </div>
                    </div>
                </div>
            </section>

            <aside><br>
                <div class="row col-md-12">
                    <div class="circulo circle15"><span class="acu">15</span><span class="spanCircle15 acu" style="margin-left: 0.5cm"><span id="pts15">500</span>&nbsp;<span class="fontPts">Pts</span></span></div>
                </div>
                <div class="row col-md-12">
                    <div class="circulo circle14"><span class="acu">14</span><span class="spanCircle14 acu" style="margin-left: 0.5cm"><span id="pts14">400</span>&nbsp;<span class="fontPts">Pts</span></span></div>
                </div>
                <div class="row col-md-12">
                    <div class="circulo circle13"><span class="acu">13</span><span class="spanCircle13 acu" style="margin-left: 0.5cm"><span id="pts13">200</span>&nbsp;<span class="fontPts">Pts</span></span></div>
                </div>
                <div class="row col-md-12">
                    <div class="circulo circle12"><span class="acu">12</span><span class="spanCircle12 acu" style="margin-left: 0.5cm"><span id="pts12">150</span>&nbsp;<span class="fontPts">Pts</span></span></div>
                </div>
                <div class="row col-md-12">
                    <div class="circulo circle11"><span class="acu">11</span><span class="spanCircle11 acu" style="margin-left: 0.5cm"><span id="pts11">120</span>&nbsp;<span class="fontPts">Pts</span></span></div>
                </div>
                <div class="row col-md-12">
                    <div class="circulo circle10"><span class="acu">10</span><span class="spanCircle10 acu" style="margin-left: 0.5cm"><span id="pts10">95</span>&nbsp;<span class="fontPts">Pts</span>&nbsp;&nbsp;Seguro</span></div>
                </div>
                <div class="row col-md-12">
                    <div class="circulo circle9"><span class="acu">09</span><span class="spanCircle9 acu" style="margin-left: 0.5cm"><span id="pts9">85</span>&nbsp;<span class="fontPts">Pts</span></span></div>
                </div>
                <div class="row col-md-12">
                    <div class="circulo circle8"><span class="acu">08</span><span class="spanCircle8 acu" style="margin-left: 0.5cm"><span id="pts8">75</span>&nbsp;<span class="fontPts">Pts</span></span></div>
                </div>
                <div class="row col-md-12">
                    <div class="circulo circle7"><span class="acu">07</span><span class="spanCircle7 acu" style="margin-left: 0.5cm"><span id="pts7">65</span>&nbsp;<span class="fontPts">Pts</span></span></div>
                </div>
                <div class="row col-md-12">
                    <div class="circulo circle6"><span class="acu">06</span><span class="spanCircle6 acu" style="margin-left: 0.5cm"><span id="pts6">55</span>&nbsp;<span class="fontPts">Pts</span></span></div>
                </div>
                <div class="row col-md-12">
                    <div class="circulo circle5"><span class="acu">05</span><span class="spanCircle5 acu" style="margin-left: 0.5cm"><span id="pts5">45</span>&nbsp;<span class="fontPts">Pts</span>&nbsp;&nbsp;Seguro</span></div>
                </div>
                <div class="row col-md-12">
                    <div class="circulo circle4"><span class="acu">04</span><span class="spanCircle4 acu" style="margin-left: 0.5cm"><span id="pts4">35</span>&nbsp;<span class="fontPts">Pts</span></span></div>
                </div>
                <div class="row col-md-12">
                    <div class="circulo circle3"><span class="acu">03</span><span class="spanCircle3 acu" style="margin-left: 0.5cm"><span id="pts3">25</span>&nbsp;<span class="fontPts">Pts</span></span></div>
                </div>
                <div class="row col-md-12">
                    <div class="circulo circle2"><span class="acu">02</span><span class="spanCircle2 acu" style="margin-left: 0.5cm"><span id="pts2">15</span>&nbsp;<span class="fontPts">Pts</span></span></div>
                </div>
                <div class="row col-md-12" style="margin-bottom: 20px;">
                    <div class="circulo circle1"><span class="acu">01</span><span class="spanCircle1 acu" style="margin-left: 0.5cm"><span id="pts1">5</span>&nbsp;<span class="fontPts">Pts</span></span></div>
                </div>
            </aside>
        </div>

        <div>
            <select id="voces" style="display: none"></select>
            <textarea id="mensaje" cols="30" rows="5" style="display: none"></textarea>
        </div>

        <!-- The Modal Ranking -->
        <div class="modal fade" id="modalRanking">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Modal body -->
                    <div class="modal-body">
                        <center>
                            <h2 class="text text-primary" style="font-weight: bold; font-family: 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif">Ranking de usuarios</h2>
                            <br>
                            <table class="table table-sm table-striped w3-center">
                                <tr>
                                    <th>Ranking</th>
                                    <th>Primer Nombre</th>
                                    <th>Primer Apellido</th>
                                    <th>Puntuación</th>
                                    <th>Tiempo</th>
                                    <th>Fecha</th>
                                </tr>
                                <?php foreach ($usuarioControl->obtenerTodos() as $usu) { ?>
                                    <tr class="selFila" id="selFila">
                                        <th class="borde marInt alCen"><?php echo ++$ranking ?></th>
                                        <td class="borde marInt alCen"><?php echo $usu->getNom1(); ?></td>
                                        <td class="borde marInt alCen"><?php echo $usu->getApe1(); ?></td>
                                        <td class="borde marInt alCen"><?php echo $usu->getAcumulado(); ?></td>
                                        <td class="borde marInt alCen"><?php echo $usu->getTiempototal(); ?></td>
                                        <td class="borde marInt alCen"><?php echo $usu->getFecha(); ?></td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </center>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- The Modal Instrucciones-->
        <div class="modal fade" id="modalInstrucciones">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">


                    <!-- Modal body -->
                    <div class="modal-body">
                        <center>
                            <h2 class="text text-primary " style="font-weight: bold; font-family: 'Trebuchet MS', 'Lucida Sans Unicode'">Instrucciones</h2>
                        </center>
                        <ol>
                            <li>El tiempo maximo para la realización del juego es de una hora, este cronometro sera el mas grande ubiocado en la esquina superior izquierda.</li>
                            <li>Iniciara quien quiere ser Matemático con la puntuación optenida de los dos ejercicios anteriores</li>
                            <li>El tiempo maximo por pregunta sera de 2 min y 30 seg, al finalizar este tiempo y si no ah marcado ninguna respuesta, se le restara 10 pts al acumulado total</li>
                            <li>Pasados 2 min y 10 seg, el cronometro por pregunta cambiara a color rojo, esto indicara que le quedan 20 seg para responder la pregunta, de no ser así, se le restaran los pts</li>
                            <li>El juego cuenta con tres opciones ayudas.</li>
                            <li>50/50 Eliminara dos opciones incorrectas.</li>
                            <li>La ayuda de Whatsapp enviara la pregunta y las respuesta en la cual se encuentra a número ingresado.</li>
                            <li>La ayuda de Whatsapp enviara la pregunta y las respuesta en la cual se encuentra a número ingresado.</li>
                            <li>No podra usar la ayuda del Whatsapp con respuestas que contengan imagenes</li>
                            <li>Una vez utilizado una ayuda se anulara.</li>
                            <li>El juego cuenta con dos seguros, Los cuales en caso de perder le aseguraran los puntos que halla acumulado hasta el seguro anterior, en caso de perder antes del primer seguro, se almacenara el acumulado incial.</li>
                        </ol>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn" style="width: 15%;" data-dismiss="modal">Cerrar</button>
                    </div>

                </div>
            </div>
        </div>

        <!-- The Modal Num Wapp-->
        <div class="modal fade" id="modalNumWapp">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header" style="background-image: url('../recursos/img/final/fondoWapp.png')">

                        <img class="col-md-4" src="../recursos/img/final/headerWapp.png" style="height: 40px;">

                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <label>Por favor ingresar el número de destino</label>
                        <input class="form-control" type="number" id="inputWapp" />
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button style="background-color: #2ED881; font-weight: bold;" id="sentWapp">Enviar</button>
                    </div>

                </div>
            </div>
        </div>

        <!-- The Modal RtaIncorrecta -->
        <div class="modal fade" id="modalRtaIncorrecta">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <img class="card-img-top" src="../recursos/img/incorrecta2.png" alt="Card image">
                    </div>

                    <!-- Modal body -->
                    <center>
                        <div class="modal-body">
                            <label id="RtaInc" style="font-family: 'times new roman'; font-size: 18px; font-weight: bold;"></label><br><br>
                            <form method="post" id="formFin">
                                <table>
                                    <tr>
                                        <td><span style="font-weight: bold; font-size: 20px; ">Acumulado:</span></td>
                                        <td><input type="text" id="acumulado" name="acumulado" class="form-control" disabled style="width: 40%;" /></td>
                                    </tr>
                                    <tr>
                                        <td><span style="font-weight: bold; font-size: 20px; ">Tiempo Total:</span> </td>
                                        <td><input type="text" id="tiempototal" name="tiempototal" class="form-control" disabled style="width: 40%;" /></td>
                                    </tr>
                                </table>
                                <br><br>
                                <input type="hidden" class="btn" id="FinalizarJuego" data-dismiss="modal" value="Finalizar" />
                            </form>
                            <!-- <label id="RtaInc"></label>
                            <label id="acu"> </label> -->
                        </div>
                    </center>



                </div>
            </div>
        </div>

        <!-- The Modal FinJuego -->
        <div class="modal fade" id="modalFinJuego">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <img class="card-img-top" src="../recursos/img/correcto.png" alt="Card image" style="width: 400px; height: 300px">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <center>
                        <div class="modal-body">
                            <label id="finJuego" style="font-family: 'times new roman'; font-size: 18px; font-weight: bold;"></label><br><br>
                            <form method="post" id="formFinBien">
                                <table>
                                    <tr>
                                        <td><span style="font-weight: bold; font-size: 20px; ">Acumulado:</span></td>
                                        <td><input type="text" id="acumulado" name="acumulado" class="form-control acumulado" disabled style="width: 40%;" /></td>
                                    </tr>
                                    <tr>
                                        <td><span style="font-weight: bold; font-size: 20px; ">Tiempo Total:</span> </td>
                                        <td><input type="text" id="tiempototal" name="tiempototal" class="form-control tiempototal" disabled style="width: 40%;" /></td>
                                    </tr>
                                </table>
                                <br><br>
                                <input type="hidden" class="btn" id="FinalizarJuegoBien" data-dismiss="modal" value="Finalizar" />
                            </form>
                            <!-- <label id="RtaInc"></label>
                            <label id="acu"> </label> -->
                        </div>
                        <!-- <div class="modal-body">
                            <label id="finJuego"></label>
                            <label id="acuFin"> </label>
                        </div> -->
                    </center>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="$(location).attr('href','HomeJuego.php')">Salir</button>
                    </div>

                </div>
            </div>
        </div>

        <div id="snackbar"><label id="alert"></label></div>
    </tbody>

    <footer class="w3-container w3-center w3-opacity">
        <div class="w3-xlarge">
            <a class="fa fa-facebook-official w3-hover-opacity" href="https://es-la.facebook.com/UniremingtonOficial/" target="_blank"></a>
            <a class="fa fa-instagram w3-hover-opacity" href="https://www.instagram.com/uniremington_medellin/" target="_blank"></a>
            <a class="fa fa-twitter w3-hover-opacity" href="https://twitter.com/Uni_Remington?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor" target="_blank"></a>
            <a class="fa fa-linkedin w3-hover-opacity" href="https://co.linkedin.com/company/uniremington" target="_blank"></a>
        </div>
        <div class="row">
            <div class="col-xs-12 col-ms-9 col-md-12 col-lg-12" id="grad">
                <center>
                    <h5 style="font-weight: bold;">www.uniremington.edu.co <br>
                        Medellín Antioquia, Colombia<br>
                        &copy;<br>
                        Acosta A., Castro A. & Mora N.</h5>
                </center>
            </div>
        </div>
    </footer>

    <script src="rutbasicas.js"></script>
    <script src="artificiales.js"></script>
    <script src="../recursos/js/script.js"> </script>
    <script src="../recursos/jsLibrerias/popper.min.js"></script>
    <script src="../recursos/jsLibrerias/bootstrap.min.js"></script>
    <script src="../recursos/jsLibrerias/jquery-3.3.1.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="../recursos/jquery-ui-1.12.1/jquery-ui.js"></script>
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"> </script>
    <script src="https://www.geogebra.org/apps/deployggb.js"></script>
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"> </script>
    <script>
        var ins = 0,
            pos = 1,
            acu = <?php print $acu12 ?>,
            alert = 0;


        $('#FinalizarJuego').click(e => {
            let time = $('#screen1').text();
            $("#formFin").attr('action', `../funciones/actualizarUsuarioFin.php?id=<?php print $id ?>&acu=${acu}&tiempototal=${time}&&nom=<?php print $nom; ?>&ape=<?php print $ape; ?>`);
            $("#formFin").submit();
        });

        $('#FinalizarJuegoBien').click(e => {
            let time = $('#screen1').text();
            $("#formFinBien").attr('action', `../funciones/actualizarUsuarioFin.php?id=<?php print $id ?>&acu=${acu}&tiempototal=${time}&&nom=<?php print $nom; ?>&ape=<?php print $ape; ?>`);
            $("#formFinBien").submit();
        });


        $('#acumuladoo').text(acu);

        window.onload = function() {
            pantalla = document.getElementById("screen1");
            pantalla2 = document.getElementById("screen2");
        }

        var isMarch = false;
        var acumularTime = 0;

        var isMarch2 = false;
        var acumularTime2 = 0;

        function cronometro() {
            timeActual = new Date();
            acumularTime = timeActual - timeInicial;
            acumularTime2 = new Date();
            acumularTime2.setTime(acumularTime);
            cc = Math.round(acumularTime2.getMilliseconds() / 10);
            ss = acumularTime2.getSeconds();
            mm = acumularTime2.getMinutes();
            hh = acumularTime2.getHours() - 18;
            if (cc < 10) {
                cc = "0" + cc;
            }
            if (ss < 10) {
                ss = "0" + ss;
            }
            if (mm < 10) {
                mm = "0" + mm;
            }
            pantalla.innerHTML = mm + " : " + ss;
        }

        function cronometro2() {
            timeActual2 = new Date();
            acumularTime4 = timeActual2 - timeInicial2;
            acumularTime5 = new Date();
            acumularTime5.setTime(acumularTime4);
            cc2 = Math.round(acumularTime5.getMilliseconds() / 10);
            ss2 = acumularTime5.getSeconds();
            mm2 = acumularTime5.getMinutes();
            hh2 = acumularTime5.getHours() - 18;
            if (cc2 < 10) {
                cc2 = "0" + cc2;
            }
            if (ss2 < 10) {
                ss2 = "0" + ss2;
            }
            if (mm2 < 10) {
                mm2 = "0" + mm2;
            }
            if ((parseInt(mm2) == 2) && (parseInt(ss2) == 30)) {
                reset2();
                $('#screen2').css('color', 'black');
                acu -= 10;
                $('#acumuladoo').text(acu);
                start2();
            }

            if ((parseInt(mm2) == 2) && (parseInt(ss2) == 10)) {
                $('#screen2').css('color', 'red');
                $('#alert').text('Queda poco tiempo!!')
                var x = document.getElementById("snackbar");
                x.className = "show";

                setTimeout(function() {
                    x.className = x.className.replace("show", "");
                }, 2000);
            }
            pantalla2.innerHTML = mm2 + " : " + ss2;

        }

        function start() {
            if (isMarch == false) {
                timeInicial = new Date();
                control = setInterval(cronometro, 10);
                isMarch = true;
            }
        }

        function start2() {
            if (isMarch2 == false) {
                timeInicial2 = new Date();
                control2 = setInterval(cronometro2, 10);
                isMarch2 = true;
            }
        }

        function stop() {
            if (isMarch == true) {
                clearInterval(control);
                isMarch = false;
            }
            if (isMarch2 == true) {
                clearInterval(control2);
                isMarch2 = false;
            }
        }

        function resume() {
            if (isMarch == false) {
                timeActu2 = new Date();
                timeActu2 = timeActu2.getTime();
                acumularResume = timeActu2 - acumularTime;

                timeInicial.setTime(acumularResume);
                control = setInterval(cronometro, 10);
                isMarch = true;
            }
        }

        function reset() {
            if (isMarch == true) {
                clearInterval(control);
                isMarch = false;
            }
            acumularTime = 0;
            pantalla.innerHTML = "00 : 00 : 00";
        }

        function reset2() {
            if (isMarch2 == true) {
                clearInterval(control2);
                isMarch2 = false;
            }
            acumularTime2 = 0;
            pantalla2.innerHTML = "00 : 00 : 00";
        }


        $('#btnWapp').click(e => {
            if ((pos = 3) || (pos = 11)) {
                $('#alert').text('No se puede usar este comodin')
                var x = document.getElementById("snackbar");
                x.className = "show";

                setTimeout(function() {
                    x.className = x.className.replace("show", "");
                }, 2000);
            } else {
                $('#modalNumWapp').modal('show');
            }
        });

        $('#sentWapp').click(e => {
            let numTmp = $('#inputWapp').val();
            let num = numTmp.toString();

            let msg = 'Pregunta:' + $(`.pre${pos}`).html();
            let ra = '' + $(`#resA${pos}`).html();
            let rb = '' + $(`#resB${pos}`).html();
            let rc = '' + $(`#resC${pos}`).html();
            let rd = '' + $(`#resD${pos}`).html();

            window.open(`https://api.whatsapp.com/send?phone=57${num}&text=${msg}%20 %20 %20 %20 %20 A [ ${ra} ]%20 %20 %20 %20 %20 B [ ${rb} ]%20 %20 %20 %20 %20 C [ ${rc} ]%20 %20 %20 %20 %20 D [ ${rd} ]`);
            $('#btnWapp').css('display', 'none');
            $('#modalNumWapp').modal('hide');
        });

        $('#btnMedio').click(e => {
            debugger;
            let res = ['A', 'B', 'C', 'D'];
            let cont = 0;
            for (let i = 0; i < res.length; i++) {
                let query = document.getElementById(`res${res[i]}${pos}`).getAttribute("data-value");
                if (query == '0') {
                    $(`#res${res[i]}${pos}`).css('display', 'none');
                    cont++;
                    if (cont == 2) break;
                }
            }
            $('#btnMedio').css('display', 'none');
        });

        function resCorrecta(numPre) {
            debugger;
            $(`.pre${numPre}`).css('display', 'none');
            $(`.res${numPre}`).css('display', 'none');

            $(`.pre${numPre + 1}`).css('display', 'block');
            $(`.res${numPre + 1}`).css('display', 'block');

            $(`.circle${numPre}`).css('background', '#28B059');
            $(`.spanCircle${numPre}`).css('color', '#28B059');

            pos++;
            acu += parseInt($(`#pts${numPre}`).text());
            $('#acumuladoo').text(acu);

            if (numPre == 15) {
                $('#finJuego').text('Felicitaciones, contestaste bien todas las preguntas');
                $('#acuFin').text(`Tu puntaje final es: ${acu}`);

                $('.acumulado').val(acu);
                let time = $('#screen1').text();
                $('.tiempototal').val(time);
                $('#modalFinJuego').modal('show');
                setTimeout(() => {
                    $("#FinalizarJuego").trigger("click");
                }, 3000);
            }
            reset2();
            start2();
        }

        function resIncorrecta(numPre) {
            debugger;
            if (numPre < 6) {
                acu = <?php print $acu12 ?>;
            } else if (numPre >= 6 && numPre < 11) {
                acu = 125 + <?php print $acu12 ?>;
            } else if (numPre == 15) {
                acu = 500 + <?php print $acu12 ?>;
            }

            $('#RtaInc').text(`Has ingresado la opción incorrecta en la pregunta número ${numPre}`);
            //$('#acu').text(`Tu acumulado es: ${acu}`);
            stop();
            $('#acumulado').val(acu);
            let time = $('#screen1').text();
            $('#tiempototal').val(time);
            $('#modalRtaIncorrecta').modal('show');
            alert = 1;
            $('#screen1').css('display', 'none');
            setTimeout(() => {
                $("#FinalizarJuego").trigger("click");
            }, 3000);

        }

        $('#btnJugar').click(e => {
            if (ins == 0) {
                $(`#snackbar`).css('background-color', '#fff');
                var x = document.getElementById("snackbar");
                x.className = "show";

                setTimeout(function() {
                    x.className = x.className.replace("show", "");
                }, 2000);
            } else {
                $('.initJuego').css('display', 'none');
                setTimeout(() => {
                    debugger;
                    $('#regresarHome').css('display', 'none');
                    $('#juegoFinal').css('display', 'block');
                    $('#screen1').css('display', 'block');
                    start();
                    start2();
                }, 1000);
            }
        });

        $('#btninstrucciones').click(e => {
            ins = 1;
        });
    </script>

</body>

</html>