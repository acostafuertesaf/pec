<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="../recursos/css/homeDocumentacion.css" />
  <link rel="stylesheet" type="text/css" href="../recursos/cssLibrerias/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../recursos/css/homeDocumentacion.css">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="icon" type="image/png" href="../recursos/img/pec5.png" />
  <title>Profesor Ecuaciones Cuadráticas</title>

  <style>
    .ir-arriba {
      display: none;
      padding: 20px;
      font-size: 20px;
      color: #fff;
      cursor: pointer;
      position: fixed;
      bottom: 20px;
      right: 20px;
    }

    h2 {
      font-family: 'times new roman';
      font-size: 45px;
    }

    h3 {
      font-family: 'times new roman';
      font-size: 35px;
    }

    p {
      font-family: 'times new roman';
      font-size: 25px;
    }

    .historia {
      padding: 10px;
      margin-top: 15px;
      border: #2e75b6 10px outset;
      border-radius: 5px;
      background-color: #2e75b6;
      color: white;
    }

    nav {
      background-color: transparent;
      margin-top: 2cm;
    }

    .textHist {
      font-size: 25px;
    }

    .login {
      color: #2e75b6;
    }

    .login:hover {
      color: black;
    }

    footer {
      width: 100%;
      bottom: 0;
      color: #000;
    }

    html {
      min-height: 100%;
      position: relative;
    }
  </style>
</head>

<body>

  <span class="ir-arriba icon-arrow-up2"><img src="../recursos/img/up.png" style="width: 60px;"></span>

  <nav class="w3-sidebar w3-bar-block w3-small w3-hide-small w3-white w3-opacity" style="display: none; width: 5.5cm;" id="menu">
    <div>
      <center><a id="oculMenu"><img src="../recursos/img/pec.png" class="rounded-circle" alt="logo" style="width:100px; height:100px">
          <pan><img src="../recursos/img/menu1.png" style="width:15px; height:15px"></span>
        </a> </center>
    </div>
    <a href="#video" class="w3-bar-item w3-button w3-padding-large w3-hover-blue" style="text-decoration: none;">
      <center><img src="../recursos/img/video.png" alt="logo" style="width:50px; height:50px">
        <p style="">VIDEOS</p>
      </center>
    </a>
    <a href="#coment" class="w3-bar-item w3-button w3-padding-large w3-hover-blue" style="text-decoration: none;">
      <center><img src="../recursos/img/coment2.png" alt="logo" style="width:50px; height:50px">
        <p>COMENTARIOS</p>
      </center>
    </a>
    <a href="#mv" class="w3-bar-item w3-button w3-padding-large w3-hover-blue" style="text-decoration: none;">
      <center><img src="../recursos/img/mision2.png" alt="logo" style="width:70px; height:50px">
        <p>MISIÓN & VISIÓN</p>
      </center>
    </a>
    <a href="#administradores" class="w3-bar-item w3-button w3-padding-large w3-hover-blue" style="text-decoration: none;">
      <center><img src="../recursos/img/info.png" alt="logo" style="width:50px; height:50px">
        <p>INFO ADMIN</p>
      </center>
    </a>
  </nav>
  </div>


  <header class="w3-container w3-top  w3-xlarge w3-padding-16 w3-white w3-opacity" style="height: 2cm">
    <span class="w3-right w3-padding" style="font-size: 18px;">Profesor de Ecuaciones Cuadráticas</span>
    <a href="../vista/php/logout.php" class="w3-right w3-padding login" style="font-size: 18px; text-decoration:none; font-weight: bold;">Iniciar Sesión</a>
    <a class="w3-left" id="menuHam" onclick="w3_open()"><img src="../recursos/img/menu.png" alt="logo" style="width:50px; height:50px"></a><span style="font-size: 15px;"></span>

  </header>

  <header class="w3-container w3-center w3-animate-top" style="padding:80px 16px; ">
    <img src="../recursos/img/pec5.png" alt="logo" style="width:600px; height:500px"><br>
    <span style="font-size: 45px; font-family: 'Times New Roman'; ">Profesor de Ecuaciones Cuadráticas</span>
  </header>

  <tbody>
    <!-- Importancia -->
    <div class="w3-row-padding  w3-padding-64 w3-container" style="background: linear-gradient(white, #B0DEFF, #2e75b6); margin-top: 8cm;">
      <div class="w3-content">
        <div class="w3-center">
          <h2 style="font-weight: bold;">Fórmula General Para Ecuaciones Cuadráticas</h2>
        </div>
        <h3 class="w3-padding-32 " style="font-size: 30px; font-weight: bold;">Introducción</h3>
        <h4>
          <p style="font-size: 30px">
            Hay varias técnicas para resolver ecuaciones de segundo grado, las cuales van
            desde el tanteo hasta la factorización. Sin embargo, existen ecuaciones cuadráticas
            que no pueden resolverse con dichas técnicas. <br>
            Existe una técnica llamada <b>Fórmula general para resolver ecuaciones cuadráticas de segundo grado</b>
            que funciona con cualquier ecuación.
          </p>
          <p style="font-size: 30px">
            Puedes resolver una ecuación cuadrática <b>completando el cuadro,</b> reescribiendo parte de la ecuación como
            un trinomio cuadrado perfecto. Si completas el cuadrado de una ecuación genérica <span><img src="../recursos/img/noGeneral-remove.png" style="width: 180px;"></span> y luego
            resuelves x, encuentras que <span><img src="../recursos/img/general3.png" style="width: 180px;"></span> esta ecuación se le conoce como ecuación cuadrática. esta fórmula
            es muy útil para resolver ecuaciones cuadráticas que son difíciles o imposibles de factorizar
            y usarla puede ser más rápido que completar el cuadrado. La fórmula cuadrática puede usarse
            para resolver cualquier ecuación de la forma <span><img src="../recursos/img/noGeneral-remove.png" style="width: 180px;"></span>.
          </p>
          <p style="font-size: 30px">
            Recuerda que una raíz cuadrada posee siempre dos valores, uno positivo y uno negativo. De
            manera que cuando utilices la fórmula general debes completar ambos signos por separado.
          </p>
        </h4>
      </div>

    </div>

    <!-- Historia -->
    <div class="row" style="background: linear-gradient(#2e75b6, #B0DEFF, white);">
      <div class="col-md-2"></div>
      <div class="col-md-2">
        <div class="row">
          <div><img src="../recursos/img/mouse.png" alt="logo" style="width:50px; height:50px"></div>
          <div><span style="color: white; margin-left: 15px">Pasar Mouse por encima<br>para desplegar la información</span></div>
        </div>

        <div class="row historia" onmouseover="historia('historia')" onmouseout="noHistoria('historia')"><span class="fontHoverMouse">Algo de Historia</span></div>

        <div class="row historia" onmouseover="historia('soluciones')" onmouseout="noHistoria('soluciones')"><span class="fontHoverMouse">Soluciones de la ecuación</span></div>

        <div class="row historia" onmouseover="historia('clasificacion')" onmouseout="noHistoria('clasificacion')"><span class="fontHoverMouse">Clasificación</span></div>
      </div>



      <div class="col-md-6 w3-center" style="border: #444444 5px double; margin-left: 80px">

        <header style="padding-bottom: 15px;">
          <center>
            <div style="border-bottom:solid 1px #D0D0D0; width: 100%;">
              <span style="font-size: 22px; font-family: 'times new roman'; font-weight: bold;">
                Panel Informativo
              </span>
            </div>
          </center>
        </header>


        <!-- Historia -->
        <div style="display: none;" id="historia">
          <p class="textHist">Las ecuaciones de segundo grado y su solución de las ecuaciones se conocen desde la antigüedad. En Babilonia se conocieron algoritmos para resolverla.
            Fue encontrado independientemente en otros lugares del mundo. En Grecia, el matemático Diofanto de Alejandría aportó un procedimiento para resolver este tipo de ecuaciones
            (aunque su método sólo proporcionaba una de las soluciones, incluso en el caso de que las dos soluciones sean positivas). La primera solución completa la desarrolló el matemático
            Al-Juarismi (o Al-Khwarizmi según otras grafías), en el siglo IX en su trabajo Compendio de cálculo por reintegración y comparación, cerrando con ello un problema que se había
            perseguido durante siglos. Basándose en el trabajo de Al-Juarismi, el matemático judeoespañol Abraham bar Hiyya, en su Liber embadorum, discute la solución de estas ecuaciones.
            [cita requerida] Hay que esperar a Évariste Galois para conseguir resolver en general las ecuaciones polinómicas, o saber cuándo son irresolubles por radicales, que viene a ser
            una generalización de los métodos de resolución de las ecuaciones de segundo grado.</p>

          <p class="textHist">La primera gran dificultad pudo surgir en la solución de ecuaciones cuadráticas se dio con la ecuación x^2 - 2 = 0 en la época de los pitagóricos,
            al calcular la longitud de la diagonal de un cuadrado de lado 1 ya que no se podía expresar la raíz cuadrada de dos como razón de dos números enteros.​</p>

          <p class="textHist">En el Renacimiento al resolver x^2 + 1 = 0 que requiere hallar un número real cuyo cuadrado sea -1, se superó con la construcción de números imaginarios
            y la invención de la unidad imaginaria i, definida mediante la igualdad i^2 = -1</p>
        </div>

        <!-- Soluciones de la ecuacion cuadrática -->
        <div style="display: none" id="soluciones">
          <p class="textHist">Para una ecuación cuadrática con coeficientes reales o complejos existen siempre dos soluciones, no necesariamente distintas, llamadas raíces,
            que pueden ser reales o complejas (si los coeficientes son reales y existen dos soluciones no reales, entonces deben ser complejas conjugadas).
            Fórmula general para la obtención de raíces:</p>
          <img src="../recursos/img/general3.png" style="width: 180px;">
          <p class="textHist">Se usa ± para indicar las dos soluciones:</p>
          <img src="../recursos/img/fGeneral1.png" style="width: 180px;">
          <img src="../recursos/img/fGeneral2.png" style="width: 180px;">
        </div>

        <!-- Clasificación -->
        <div style="display: none" id="clasificacion">
          <p class="textHist" style="font-weight: bold;">La ecuación de segundo grado se clasifica de la siguiente manera</p>

          <div class="row">
            <div class="col-md-4">
              <p class="textHist" style="font-weight: bold;">Completa</p>
              <img src="../recursos/img/noGeneral-remove.png" style="width: 150px;">
              <p class="textHist">donde los tres coeficientes a, b y c son distintos de cero</p>
              <p class="textHist">Esta ecuación admite tres maneras para las soluciones: dos números reales y diferentes, dos números reales e
                iguales (un número real doble), o dos números complejos conjugados, dependiendo del valor que tome el discriminante</p>
              <img src="../recursos/img/completa.png">
              <p class="textHist">ya sea positivo, cero o negativo, respectivamente.</p>
              <p class="textHist">Se resuelven por factorización, por el método de completar el cuadrado o por fórmula general. La fórmula general se deduce más adelante.</p>
            </div>

            <div class="col-md-4">
              <p class="textHist" style="font-weight: bold;">Incompleta pura</p>
              <img src="../recursos/img/incompleta.png">
              <p class="textHist">donde los valores de a y de c son distintos de cero. Se resuelve despejando x con operaciones inversas y su solución
                son dos raíces reales que difieren en el signo si los valores de a y c tienen signo contrario o bien dos números imaginarios puros que difieren
                en el signo si los valores de a y c tienen el mismo signo. Una ecuación cuadrática incompleta de la forma:</p>
              <img src="../recursos/img/incompleta2.png">
              <p class="textHist">con a distinto de cero, muy rara vez aparece en la práctica y su única solución de multiplicidad dos es, por supuesto, x = 0</p>
            </div>

            <div class="col-md-4">
              <p style="font-size: 22px; font-weight: bold;">Incompleta mixta</p>
              <img src="../recursos/img/incompletaMixta.png">
              <p style="font-size: 22px">donde los valores de a y de b son distintos al numero cero. Se resuelve por factorización de x y siempre tiene la solución trivial x1 = 0.
                No tiene solución en números imaginarios.</p>


            </div>
          </div>


        </div>


      </div>
    </div><br><br><br>

    <!-- Objetivos -->
    <div class="w3-row-padding  w3-padding-64 w3-container" style="background: linear-gradient(white, #B0DEFF, #2e75b6);">
      <div class="w3-content">
        <div class="w3-twothird">
          <h2 style="font-weight: bold;">Fórmula General Para Ecuaciones Cuadráticas</h2>
          <div class="w3-center">
            <h3 class="w3-padding-32" style="font-weight: bold;">Objetivos</h3>
          </div>
          <p>
            <ol style="font-size: 20px">
              <li>Escribir una ecuación cuadrática en su forma estándar identificando los valores de a, b, c en la forma estándar de una ecuación cuadrática.</li>
              <li>Usar la fórmula cuadrática para encontrar todas las soluciones reales.</li>
              <li>Usar la fórmula cuadrática para encontrar todas las soluciones complejas</li>
              <li>Calcular el discriminante e indicar el número y tipo de soluciones</li>
              <li>Resolver problemas de aplicación que requieren el uso de la fórmula cuadrática</li>
            </ol>
          </p>
        </div>

        <div class="w3-third w3-center">
          <span><img src="../recursos/img/generalEjm6.png" style="width: 250px;"></span>
        </div>
      </div>
    </div>


    <!-- De donde sale -->
    <div class="w3-row-padding  w3-padding-64 w3-container" style="background: linear-gradient(#2e75b6, #B0DEFF, white);">
      <div class="w3-content">
        <div class="w3-center">
          <h2 class="w3-padding-32" style="font-weight: bold">¿De dónde Proviene?</h2>
        </div>
        <div class="color1 col-xs-12 col-ms-12 col-md-12">
          <iframe width="1100" height="500" src="https://www.youtube.com/embed/2dZM7BAs6FE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style=" width 1100px; height: 500px;"></iframe>
        </div>
      </div>
    </div>

    <!-- Importancia -->
    <div class=" w3-container" style="background: linear-gradient(white, #B0DEFF, #2e75b6);">
      <div class="w3-content">
        <div class="w3-center">
          <h2 style="font-weight: bold">Importancia de las ecuaciones en la cotidianidad</h2>
          <br>
        </div>
        <h5>
          <p>
            Las funciones cuadráticas son más que curiosidades algebraicas — son ampliamente usadas en la ciencia, los negocios, y la ingeniería. La parábola con forma de U puede describir
            trayectorias de chorros de agua en una fuente y el botar de una pelota, o pueden ser incorporadas en estructuras como reflectores parabólicos que forman la base de los platos
            satelitales y faros de los carros. Las funciones cuadráticas ayudan a predecir ganancias y pérdidas en los negocios, graficar el curso de objetos en movimiento, y asistir en la
            determinación de valores mínimos y máximos. Muchos de los objetos que usamos hoy en día, desde los carros hasta los relojes, no existirían si alguien, en alguna parte, no hubiera
            aplicado funciones cuadráticas para su diseño.
          </p>
          <p>
            Comúnmente usamos ecuaciones cuadráticas en situaciones donde dos cosas se multiplican juntas y ambas dependen de la misma variable. Por ejemplo, cuando trabajamos con un área.
            Si ambas dimensiones están escritas en términos de la misma variable, usamos una ecuación cuadrática. Porque la cantidad de un producto vendido normalmente depende del precio, a veces
            usamos una ecuación cuadrática para representar las ganancias como un producto del precio y de la cantidad vendida. Las ecuaciones cuadráticas también son usadas donde se trata con la gravedad,
            como por ejemplo la trayectoria de una pelota o la forma de los cables en un puente suspendido.
          </p>
        </h5>

        <br>
        <div class="row">
          <center>
            <div style="width: 1200px;">
              <div class="w3-card-4" style="width:70%">
                <header class="w3-container w3-light-grey"></header>
                <div class="w3-container">
                  <h3 style="font-weight: bold">Tiro Parabólico</h3>
                  <hr>

                  <img src="../recursos/img/tiro2.png" style="width: 300px;"><br>
                </div><br>
                <button type="button" class="w3-button w3-block w3-dark-grey btnTiro" onclick="ampliar('Tiro')">Ampliar</button>

                <div class="w3-container">
                  <div style="display: none" id="ampliarTiro">
                    <h5>
                      <p>Una aplicación muy común y fácil de entender de una función cuadrática es la trayectoria seguida por objetos lanzados hacia arriba y con cierto ángulo. En estos casos,
                        la parábola representa el camino de la pelota (o roca, o flecha, o lo que se haya lanzado). Si graficamos la distancia en el eje x y la altura en el eje y, la distancia que
                        del lanzamiento será el valor de x cuando y es cero. Este valor es una de las raíces de una ecuación cuadrática, o intersecciones en x, de la parábola. Sabemos cómo encontrar
                        las raíces de una ecuación cuadrática — ya sea factorizando, completando el cuadrado, o aplicando la fórmula cuadrática.
                      </p>

                      <p>Consideremos el tiro hecho por un lanzador de peso. Nota que x = 0 cuando el lanzador tiene el tiro (una bola de metal pesada= en su mano — el tiro aún no ha salido.
                        El lanzador usualmente comienza con el tiro en su hombro, entonces y (la altura) no es 0 cuando x = 0:
                      </p>
                    </h5>

                    <button type="button" class="w3-button w3-block w3-dark-grey btnEjmTiro" onclick="ejm('Tiro')">Ver ejemplo</button>

                    <div style="display: none" id="ejmTiro">
                      <img src="../recursos/img/ejmTiro.png" style="width: 800px;"><br>
                      <img src="../recursos/img/ejmTiro1.png" style="width: 800px;"><br>
                      <p>SOLUCIÓN: Aproximadamente 46,4 pies</p>
                    </div>

                  </div>
                </div>
              </div><br><br>




          </center>
        </div>
        <br><br>
        <div class="row w3-center">
          <center>
            <div style="width: 1200px;">
              <div class="w3-card-4" style="width:70%">
                <header class="w3-container w3-light-grey"></header>
                <div class="w3-container">
                  <h3 style="font-weight: bold">Averiguar maximo vertical de un lanzamiento</h3>
                  <hr>

                  <img src="../recursos/img/vertical5.png" style="width: 300px;"><br>
                </div><br>
                <button type="button" class="w3-button w3-block w3-dark-grey btnVertical" onclick="ampliar('Vertical')">Ampliar</button>

                <div class="w3-container">
                  <div style="display: none" id="ampliarVertical">
                    <p>Otro uso común de las ecuaciones cuadráticas en aplicaciones del mundo real es encontrar el valor máximo (el mayor o más alto) o el mínimo
                      (el menor o más bajo) de algo. Recuerda que el vértice es el punto donde una parábola da la vuelta. Para una parábola que abre hacia abajo,
                      el vértice es el punto más alto, lo que ocurre al máximo valor posible de y. Para una parábola que abre hacia abajo, el vértice es el punto más bajo de la parábola,
                      y ocurre al mínimo valor de y.
                    </p>

                    <p>Para encontrar el máximo o el mínimo con una ecuación cuadrática, usualmente queremos poner la ecuación cuadrática en su forma vértice de una ecuación cuadrática,
                      . Esto nos permite rápidamente identificar las coordenadas del vértice (h, k).
                    </p>

                    <p>
                      Veamos cómo funciona esto con un problema de movimiento. La ecuación es comúnmente usada para modelar un objeto que ha sido lanzado o aventado.
                      La variable h representa la altura en pies, y t representa el tiempo en segundos. Los otros dos valores son números generalmente dados: h0 es la altura
                      inicial en pies y v0 es la velocidad inicial en pies/segundo.
                    </p>

                    <p>
                      Cuando trabajamos con esta ecuación, asumimos que el objeto está en "caída libre", lo que significa que se mueve sólo bajo la influencia de la gravedad.
                      No hay resistencia contra el aire u otra interferencia de ningún tipo (no tan parecido al mundo real, pero de todos modos, estas ecuaciones son útiles).
                    </p>

                    <button type="button" class="w3-button w3-block w3-dark-grey btnEjmVertical" onclick="ejm('Vertical')">Ver ejemplo</button>

                    <div style="display: none" id="ejmVertical">
                      <img src="../recursos/img/ejmVertical.png" style="width: 800px;"><br>
                      <img src="../recursos/img/ejmVertical1.png" style="width: 800px;"><br>

                    </div>

                  </div>
                </div>
              </div><br><br>
          </center>
        </div>
      </div>
    </div>

    <!-- Videos -->
    <div class="w3-row-padding w3-padding-64 w3-container" style="background: linear-gradient(#2e75b6, #B0DEFF, white);">
      <div class="container-fluid ">
        <div class="w3-center">
          <h2 id="video" style="font-weight: bold">Videos</h2>
        </div>
        <div>
          <center>
            <br>
            <div>
              <iframe src="https://www.youtube.com/embed/xmzG2xR-oBI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="width:350px;height: 200px; margin-right: 40px;"></iframe>
              <iframe src="https://www.youtube.com/embed/BxrJmKdPHRs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="width:350px;height: 200px; margin-right: 40px;"></iframe>
              <iframe src="https://www.youtube.com/embed/-qq8Vsxjr4w" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="width:350px;height: 200px; margin-right: 40px;"></iframe>
              <iframe src="https://www.youtube.com/embed/KJxWGwObrmY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="width:350px;height: 200px;"></iframe>
            </div>
            <div>
              <iframe src="https://www.youtube.com/embed/BmbaH4r1TIM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="width:350px;height: 200px;  margin-right: 40px;"></iframe>
              <iframe src="https://www.youtube.com/embed/BmbaH4r1TIM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="width:350px;height: 200px;  margin-right: 40px;"></iframe>
              <iframe src="https://www.youtube.com/embed/QHg_kzE2hcU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="width:350px;height: 200px;  margin-right: 40px;"></iframe>
              <iframe src="https://www.youtube.com/embed/GZsRzXAHHSg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="width:350px;height: 200px;"></iframe>
            </div>
            <div>
              <iframe src="https://www.youtube.com/embed/11ByL5VAwzo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="width:350px;height: 200px;  margin-right: 40px;"></iframe>
              <iframe src="https://www.youtube.com/embed/0BZf7cXpDEo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="width:350px;height: 200px;  margin-right: 40px;"></iframe>
              <iframe src="https://www.youtube.com/embed/Cx_4GgJNCwg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="width:350px;height: 200px;  margin-right: 40px;"></iframe></iframe>
              <iframe class="video" src="https://www.youtube.com/embed/R3UoCe-r6aM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="width:350px;height: 200px; "></iframe>
            </div>
            <div>
              <iframe src="https://www.youtube.com/embed/1A5KmDoGzSA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="width:350px;height: 200px;  margin-right: 40px;"></iframe>
              <iframe width="560" height="315" src="https://www.youtube.com/embed/BgJgMfzfYKY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="width:350px;height: 200px;  margin-right: 40px;"></iframe>
              <iframe width="560" height="315" src="https://www.youtube.com/embed/BgJgMfzfYKY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="width:350px;height: 200px;  margin-right: 40px;"></iframe>
              <iframe width="560" height="315" src="https://www.youtube.com/embed/SE7ZyEkPkgA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="width:350px;height: 200px; "></iframe>
            </div>
            <div>
              <iframe width="560" height="315" src="https://www.youtube.com/embed/Wj4cHg8oHzI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="width:350px;height: 200px;  margin-right: 40px;"></iframe>
              <iframe width="560" height="315" src="https://www.youtube.com/embed/gnAdna_tLK0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="width:350px;height: 200px;  margin-right: 40px;"></iframe>
              <iframe width="560" height="315" src="https://www.youtube.com/embed/qBEigKQhmXI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="width:350px;height: 200px;  margin-right: 40px;"></iframe>
              <iframe width="560" height="315" src="https://www.youtube.com/embed/QJpRDk2zeV4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="width:350px;height: 200px; "></iframe>
            </div>
          </center>
        </div>
      </div>
    </div>

    <!-- Mision & vision -->
    <div class="row" style="background: linear-gradient(white, #B0DEFF, #2e75b6); padding-bottom: 3cm;">
      <div class="col-md-3"></div>
      <div class="col-md-3">
        <h3 id="mv">
          <center style="font-weight: bold; font-family: times new roman; font-size: 35px">Misión<br></center>
        </h3>
        <span style="font-size: 20px; text-align: justify;">PEC esta diseñado y desarrollado para mejorar el aprendizaje de las Ecuaciones Cuadráticas en los estudiantes de 9° grado, a su vez que comprendan para que pueden
          servir dichas ecuaciones en la vida real y para finalizar queremos ayudarle al docente para que los estudiantes autoestudien jugando y en las clases de un mejor rendimiento.</span>
      </div>
      <div class="col-md-3">
        <h3>
          <center style="font-weight: bold; font-family: times new roman; font-size: 35px">Visión<br></center>
        </h3>
        <center><span style="font-size: 20px; text-align: justify;">PEC en el 2022 sera una aplicación web utilizada en todos las instituciones educativas a nivel nacional. </span></center>
      </div>
    </div>

    <!-- Info admin -->
    <div class="row" style="background: linear-gradient(#2e75b6, #B0DEFF, white);">
      <div class="col-md-2"></div>
      <div class="col-md-3">
        <div class="w3-card-4" id="administradores" style="width:70%">
          <header class="w3-container w3-light-grey">
            <h3 class="w3-center" style="color: #2e75b6; font-weight: bold">Angie Castro</h3>
          </header>
          <div class="w3-container">
            <p class="w3-center" style="color: #000">Ingeniera de Sistemas en Formación</p>
            <hr>
            <p>Estudiante de 6to semestre de Tecnología en Desarrollo de Software<br> <a href="https://www.uniremington.edu.co/" target="_blank" style="text-decoration: none; color: black">
                Corporación Universitaria Remington</a> <a href="https://www.uniremington.edu.co/" target="_blank" style="font-size: 12px;">Sitio Web<a><br><b>Contacto:</b> angie.kxtro@gmail.com </p><br>
          </div>
        </div>
      </div>

      <div class="col-md-3">
        <div class="w3-card-4" style="width:70%">
          <header class="w3-container w3-light-grey">
            <h3 class="w3-center" style="color: #2e75b6; font-weight: bold">Neftaly Mora</h3>
          </header>
          <div class="w3-container">
            <p class="w3-center" style="color: #000">Ingeniero de Sistemas en Formación</p>
            <hr>
            <p>Estudiante de 8vo semestre de Ingenieria de Sistemas<br> <a href="https://www.uniremington.edu.co/" target="_blank" style="text-decoration: none; color: black">
                Corporación Universitaria Remington</a> <a href="https://www.uniremington.edu.co/" target="_blank" style="font-size: 12px;">Sitio Web<a><br> <b>Contacto:</b> n.mora27@gmail.com </p><br><br><br>
          </div>

        </div>
      </div>

      <div class="col-md-3">
        <div class="w3-card-4" style="width:70%">
          <header class="w3-container w3-light-grey">
            <h3 class="w3-center" style="color: #2e75b6; font-weight: bold">Andres Acosta</h3>
          </header>
          <div class="w3-container">
            <p class="w3-center" style="color: #000">Ingeniero de Sistemas en Formación</p>
            <hr>

            <p>Estudiante de 6to semestre de Tecnología en Desarrollo de Software<br> <a href="https://www.uniremington.edu.co/" target="_blank" style="text-decoration: none; color: black">
                Corporación Universitaria Remington</a> <a href="https://www.uniremington.edu.co/" target="_blank" style="font-size: 12px;">Sitio Web<a><br> <b>Contacto:</b> acostafuertesaf@gmail.com </p><br>
          </div>

        </div>
      </div>

    </div>
  </tbody>



  <footer class="w3-container w3-padding-64 w3-center w3-opacity">
    <div class="w3-xlarge w3-padding-32">
      <a class="fa fa-facebook-official w3-hover-opacity" href="https://es-la.facebook.com/UniremingtonOficial/" target="_blank"></a>
      <a class="fa fa-instagram w3-hover-opacity" href="https://www.instagram.com/uniremington_medellin/" target="_blank"></a>
      <a class="fa fa-twitter w3-hover-opacity" href="https://twitter.com/Uni_Remington?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor" target="_blank"></a>
      <a class="fa fa-linkedin w3-hover-opacity" href="https://co.linkedin.com/company/uniremington" target="_blank"></a>
    </div>
    <div class="row">
      <div class="col-xs-12 col-ms-9 col-md-12 col-lg-12" id="grad">
        <center>
          <h5 style="font-weight: bold">www.uniremington.edu.co <br>
            Medellín Antioquia, Colombia<br>
            &copy;<br>
            Acosta A., Castro A. & Mora N.</h5>
        </center><br>
      </div>
    </div>

  </footer>

  <script src="../recursos/js/script.js"> </script>
  <script src="../recursos/jsLibrerias/popper.min.js"></script>
  <script src="../recursos/jsLibrerias/bootstrap.min.js"></script>
  <script src="../recursos/jsLibrerias/jquery-3.3.1.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <script src="../recursos/jquery-ui-1.12.1/jquery-ui.js"></script>
  <script src="https://canvasjs.com/assets/script/canvasjs.min.js"> </script>
  <script src="https://www.geogebra.org/apps/deployggb.js"></script>
  <script src="https://canvasjs.com/assets/script/canvasjs.min.js"> </script>

  <script>
    // Used to toggle the menu on small screens when clicking on the menu button

    $('#oculMenu').click(e => {
      $('#menu').css('display', 'none');
      setTimeout(() => {
        $('#menuHam').css('display', 'block');
      }, 500);


    });

    function ampliar(opc) {
      debugger;
      switch (opc) {
        case 'Tiro':
          if ($(".btnTiro").html() == 'Ampliar') {
            $(".btnTiro").html('Minimizar');
            $("#ampliarTiro").css('display', 'block')
          } else {
            $(".btnTiro").html('Ampliar');
            $("#ampliarTiro").css('display', 'none')
          }
          break;

        case 'Vertical':
          if ($(".btnVertical").html() == 'Ampliar') {
            $(".btnVertical").html('Minimizar');
            $("#ampliarVertical").css('display', 'block')
          } else {
            $(".btnVertical").html('Ampliar');
            $("#ampliarVertical").css('display', 'none')
          }
          break;


      }
    }

    function ejm(opc) {
      debugger;
      //$(`#ejm${opc}`).css('display', 'block');
      switch (opc) {
        case 'Tiro':
          if ($(".btnEjmTiro").html() == 'Ver ejemplo') {
            $(".btnEjmTiro").html('Minimizar ejemplo');
            $(`#ejm${opc}`).css('display', 'block');
          } else {
            $(".btnEjmTiro").html('Ver ejemplo');
            $(`#ejm${opc}`).css('display', 'none');
          }
          break;

        case 'Vertical':
          if ($(".btnEjmVertical").html() == 'Ver ejemplo') {
            $(".btnEjmVertical").html('Minimizar ejemplo');
            $(`#ejm${opc}`).css('display', 'block');
          } else {
            $(".btnEjmVertical").html('Ver ejemplo');
            $(`#ejm${opc}`).css('display', 'none');
          }
          break;


      }
    }

    function w3_open() {
      $('#menuHam').css('display', 'none');
      setTimeout(() => {
        //document.getElementById("mySidebar").style.display = "block";
        //document.getElementById("myOverlay").style.display = "block";
        $('#menu').css('display', 'block');

      }, 500);
    }

    function w3_close() {
      document.getElementById("mySidebar").style.display = "none";
      document.getElementById("myOverlay").style.display = "none";
    }

    // Modal Image Gallery
    function onClick(element) {
      document.getElementById("img01").src = element.src;
      document.getElementById("modal01").style.display = "block";
      var captionText = document.getElementById("caption");
      captionText.innerHTML = element.alt;
    }


    $(document).ready(function() {

      $('.ir-arriba').click(function() {
        $('body, html').animate({
          scrollTop: '0px'
        }, 300);
      });

      $(window).scroll(function() {
        if ($(this).scrollTop() > 0) {
          $('.ir-arriba').slideDown(300);
        } else {
          $('.ir-arriba').slideUp(300);
        }
      });

    });

    function historia(hist) {
      $(`#${hist}`).css('display', 'block');
    }

    function noHistoria(hist) {
      $(`#${hist}`).css('display', 'none');
    }
  </script>

</body>

</html>