<?php

require_once '../control/UsuarioControl.php';
$usuarios = [];
$usuarioControl = new UsuarioControl();
$ranking = 0;

$nom = $_GET['nom'];
$ape = $_GET['ape'];
$id = $_GET['id'];


?>

<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ranking PEC</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" type="text/css" href="../recursos/cssLibrerias/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="icon" type="image/png" href="../recursos/img/pec5.png" />
    <style>
        footer {
            width: 100%;
            position: absolute;
            bottom: 0;
            color: #000;
        }

        html {
            min-height: 100%;
            position: relative;
        }

        th {
            font-size: 22px;
            font-family: 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif"

        }

        td {
            font-size: 20px;
            font-weight: normal;
        }

        .return {
            color: #2e75b6;
        }

        .return:hover {
            color: black;
        }
    </style>
</head>


<body style="background: linear-gradient(white, #B0DEFF, #2e75b6); padding-bottom: 7cm;">

    <header class="w3-container w3-top  w3-xlarge w3-padding-16" style="height: 70px;">
        <span class="w3-right w3-padding" style="font-size: 22px; font-weight: bold;">Profesor Ecuaciones Cuadráticas</span>
        <a href="home.php?nom=<?php print $nom; ?>&ape=<?php print $ape; ?>&id=<?php print $id; ?>" class="w3-right w3-padding return" style="font-size: 22px; font-weight: bold; text-decoration: none;"><span><img src="../recursos/img/regresar.png" style="width: 20px;" /> Regresar</span></a>
        <div class="row" style="margin-left: 1cm">
            <div>
                <a id="btnPdf"><img src="../recursos/img/pdf.png" style="width: 50px; margin-right: 1cm;" /></a>
                <div style="font-size: 15px"><span><img class="zoom" src="../recursos/img/dowland.png" style="width: 12px;" /></span>Pdf</div>
            </div>
            <div>
                <a href="../Informes/usuario/usuarioWord.php"><img src="../recursos/img/word.png" style="width: 50px; margin-right: 1cm;" /></a>
                <div style="font-size: 15px"><span><img class="zoom" src="../recursos/img/dowland.png" style="width: 12px;" /></span>Word</div>
            </div>
            <div><a href="../Informes/usuario/usuarioExcel.php"><img src="../recursos/img/excel.png" style="width: 50px;" /></a>
                <div style="font-size: 15px"><span><img class="zoom" src="../recursos/img/dowland.png" style="width: 12px;" /></span>Excel</div>
            </div>
        </div>
    </header>

    <div class="container">

        <center><a href="home.php"><img src="../recursos/img/pec2.png" class="rounded-circle center-block" width="350px" height="320px"></a><br>

            <h2 class="text text-primary" style="font-weight: bold; font-family: 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif">Ranking de usuarios</h2><br><br>

            <table class="table table-sm table-striped w3-center" style="width: 80%;">
                <tr>
                    <th>Ranking</th>
                    <th>Primer Nombre</th>
                    <th>Primer Apellido</th>
                    <th>Puntuación</th>
                    <th>Tiempo</th>
                    <th>Fecha</th>
                </tr>
                <?php foreach ($usuarioControl->obtenerTodos() as $usu) { ?>
                    <tr class="selFila" id="selFila">
                        <th class="borde marInt alCen"><?php echo ++$ranking ?></th>
                        <td class="borde marInt alCen"><?php echo $usu->getNom1(); ?></td>
                        <td class="borde marInt alCen"><?php echo $usu->getApe1(); ?></td>
                        <td class="borde marInt alCen"><?php echo $usu->getAcumulado(); ?></td>
                        <td class="borde marInt alCen"><?php echo $usu->getTiempototal(); ?></td>
                        <td class="borde marInt alCen"><?php echo $usu->getFecha(); ?></td>
                    </tr>
                <?php } ?>

            </table>
        </center>
    </div>
    <footer class="w3-container w3-center w3-opacity">
        <div class="w3-xlarge w3-padding-32">
            <a class="fa fa-facebook-official w3-hover-opacity" style="color: white; text-decoration: none;" href="https://es-la.facebook.com/UniremingtonOficial/" target="_blank"></a>
            <a class="fa fa-instagram w3-hover-opacity" style="color: white; text-decoration: none;" href="https://www.instagram.com/uniremington_medellin/" target="_blank"></a>
            <a class="fa fa-twitter w3-hover-opacity" style="color: white; text-decoration: none;" href="https://twitter.com/Uni_Remington?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor" target="_blank"></a>
            <a class="fa fa-linkedin w3-hover-opacity" style="color: white; text-decoration: none;" href="https://co.linkedin.com/company/uniremington" target="_blank"></a>
        </div>
        <div class="row">
            <div class="col-xs-12 col-ms-9 col-md-12 col-lg-12" id="grad">
                <center>
                    <h5 style="font-weight: bold; color: white">www.uniremington.edu.co <br>
                        Medellín Antioquia, Colombia<br>
                        &copy;<br>
                        Acosta A., Castro A. & Mora N.</h5>
                </center><br>
            </div>
        </div>
    </footer>


    <script type="text/javascript" src="../recursos/jsLibrerias/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="../recursos/jsLibrerias/popper.min.js"></script>
    <script type="text/javascript" src="../recursos/jsLibrerias/bootstrap.min.js"></script>
    <script>
        $('#btnPdf').click(e => {
            let mywindow = window.open(`${ href="../Informes/pdf/reporteUsuarios.php"}#/`, 'print', 'height=700,width=900');
        });
    </script>

</body>

</html>