<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Inicio de Sesión PEC</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="../recursos/cssLibrerias/bootstrap.min.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="icon" type="image/png" href="../recursos/img/pec5.png" />
    <style>
        .form-control {
            width: 90%;
            height: 50px;
            margin-bottom: 20px;
            border-radius: 5px
        }

        fieldset {
            margin-top: 50px;
            margin-bottom: 30px;
            border: 2px black solid;
            border-radius: 110px;
        }
        
        footer {
            width: 100%;
            position: absolute;
            bottom: 0;
            color: #000;
        }

        h3,
        h4 {
            color: black;
            text-align: center;
        }

        .Input {
            transition: .8s;
            background-color: rgba(255, 255, 255, .255);
            color: black;
            border-color: #006;
            border-bottom-color: black;
            border-bottom-style: groove;
            border-left: none;
            border-right: none;
            border-top: none;
            border-width: 4px;
        }

        .Input:hover {
            transition: .8s;
            background-color: rgba(55, 71, 79, .5);
            box-shadow: inset;
            border-bottom-color: red;
        }

        .simple {
            text-decoration: none;
            color: white;
        }

        .simple:hover {
            color: white;
            text-decoration: none;
        }

        .Input:focus {
            transition: .8s;
            border-bottom-color: blue;
        }

        p {
            color: black;
            font-family: Helvetica;
            text-align: left;
        }

        .label {
            text-align: left;
        }

        @media screen and (max-width:1050px) {
            .logo {
                height: 520px;
            }

            .formulario {
                transition: 2s;
                width: 100%;
                margin-top: 10px;
            }
        }

        html {
            min-height: 100%;
            position: relative;
        }

    
    </style>
</head>

<body style="background: linear-gradient(#2e75b6, #B0DEFF, white); ">

    <header class="w3-container w3-top  w3-xlarge w3-padding-16 w3-opacity" style="height: 2cm">
        <span class="w3-right w3-padding" style="font-size: 18px; color: white; font-weight: bold;">Profesor de Ecuaciones Cuadráticas</span>
        <a href="homeDocumentacion.php" class="w3-right w3-padding return" style="font-size: 18px; font-weight: bold; text-decoration: none; color: white"><span><img src="../recursos/img/regresar.png" style="width: 20px;" /> Regresar</span></a>
    </header>

    <div class=" container formulario">
        <center>
            <fieldset class="col-xs-3 col-md-6" style="background: linear-gradient(white, #B0DEFF, #2e75b6);">
                <center><img src="../recursos/img/pec2.png" class="rounded-circle center-block" style="width:310px; height: 330;">
                    <legend class=" hidden-xs">
                        <h3 style="font-size: 55px; font-family: times new roman, Geneva, Tahoma, sans-serif; color: #black">Iniciar Sesión</h3><br>

                    </legend>
                    <div class="row">
                        <div class="col-md-12"></div>
                        <div class="col-md-12">
                            <form role="form" name="login" action="php/login.php" method="post">
                                <div class="form-group">
                                    <!-- <h4 style="font-size: 30px">Usuario</h4> -->
                                    <input type="text" class="form-control" id="username" name="username" placeholder="Usuario" required style="border:#000 1px solid">
                                    <small class="form-text text-danger" id="msgUsername"></small>
                                </div>
                                <div class="form-group">
                                    <!-- <h4 style="font-size: 30px">Contraseña</h4> -->
                                    <input type="password" class="form-control" id="password" name="password" placeholder="Contrase&ntilde;a" required style="border:#000 1px solid">
                                </div><br>
                                <div><button class="btn center-block" type="submit" id="sent" style="width: 220px; height: 50px; border: black 1px solid; margin-right: 15px; border-radius: 5px; color: black; font-size: 16px; font-weight: bold; background: #CFCFCF;">Aceptar</span></button></div><br>

                                <div><a href="../vista/registro.php" class="w3-right" id="cuenta" style="margin-right: 1.5cm; text-decoration:none; color: white">Crear Cuenta</a></div>
                            </form>
                            <br>

                        </div>
                    </div>
            </fieldset>
        </center>
    </div>

    <footer class="w3-container w3-center w3-opacity">
        <div class="w3-xlarge w3-padding-32">
            <a class="fa fa-facebook-official w3-hover-opacity" href="https://es-la.facebook.com/UniremingtonOficial/" target="_blank"></a>
            <a class="fa fa-instagram w3-hover-opacity" href="https://www.instagram.com/uniremington_medellin/" target="_blank"></a>
            <a class="fa fa-twitter w3-hover-opacity" href="https://twitter.com/Uni_Remington?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor" target="_blank"></a>
            <a class="fa fa-linkedin w3-hover-opacity" href="https://co.linkedin.com/company/uniremington" target="_blank"></a>
        </div>
        <div class="row">
            <div class="col-xs-12 col-ms-9 col-md-12 col-lg-12" id="grad">
                <center >
                    <h5 style="font-weight: bold;">www.uniremington.edu.co <br>
                        Medellín Antioquia, Colombia<br>
                        &copy;<br>
                        Acosta A., Castro A. & Mora N.</h5>
                </center><br>
            </div>
        </div>
    </footer>


    <!-- <script src="../recursos/jsLibrerias/valida_login.js"></script> -->
    <script src="../recursos/jsLibrerias/jquery-3.3.1.min.js"></script>
    <script src="../recursos/jsLibrerias/popper.min.js"></script>
    <script src="../recursos/jsLibrerias/bootstrap.min.js"></script>

    <script>

    </script>

</body>

</html>