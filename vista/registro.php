<?php

require_once '../control/UsuarioControl.php';
$usuarioControl = new UsuarioControl();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Registro PEC</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" type="text/css" href="../recursos/cssLibrerias/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="icon" type="image/png" href="../recursos/img/pec5.png" />
    <style>
        .form-control {
            width: 90%;
            height: 45px;
            margin-bottom: 10px;
            border: 1px black solid;
            border-radius: 5px
        }

        .logo {
            height: 120px;
        }

        h3,
        h4 {
            color: black;
            text-align: center;
            font-family: Helvetica;
        }

        .Input:hover {
            box-shadow: inset;
            border-bottom-color: red;
        }

        .simple {
            text-decoration: none;
            color: white;
        }


        .simple:hover {
            color: white;
            text-decoration: none;
        }

        p {
            color: black;
            font-family: Helvetica;
            text-align: left;
        }

        .label {
            text-align: left;
        }

        footer {
            width: 100%;
            position: absolute;
            bottom: 0;
            color: #000;
        }

        html {
            min-height: 100%;
            position: relative;
        }

        fieldset {
            margin-top: 5px;
            border: 2px black solid;
            border-radius: 110px;
        }
    </style>
</head>


<body style="background: linear-gradient(#2e75b6, #B0DEFF, white); padding-bottom: 7cm;">

    <header class="w3-container w3-top  w3-xlarge w3-padding-16 w3-opacity" style="height: 2cm">
        <span class="w3-right w3-padding" style="font-size: 20px; color: white; font-weight: bold;">Profesor de Ecuaciones Cuadráticas</span>
        <a href="login.php" class="w3-right w3-padding return" style="font-size: 22px; font-weight: bold; text-decoration: none; color: white"><span><img src="../recursos/img/regresar.png" style="width: 20px;" /> Regresar</span></a>
    </header>

    <div class=" container">
        <center>
            <fieldset class="col-xs-3 col-md-6" style="background: linear-gradient(white, #B0DEFF, #2e75b6);">

                <center><img src="../recursos/img/pec2.png" class="rounded-circle center-block" style="width:310px; height: 330;" />

                    <legend class=" hidden-xs">
                        <h3 style="font-size: 55px; font-family: times new roman, Geneva, Tahoma, sans-serif; color: #black">Registro</h3><br>
                    </legend>

                    <div class="row">
                        <div class="col-md-12">
                            <form role="form" action="../funciones/actualizarUsuario.php" method="post">
                                <div class="row ">
                                    <div class="col-md-6">
                                        <input type="text" id="nom1" name="nom1" class="form-control" placeholder="* Primer Nombre" required style="width: 100%;" />
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" id="nom2" name="nom2" class="form-control" placeholder="Segundo Nombre" style="width: 100%;">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" id="ape1" name="ape1" class="form-control" placeholder="* Primer Apellido" required style="width: 100%;">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" id="ape2" name="ape2" class="form-control" placeholder="Segundo apellido" style="width: 100%;">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="email" id="email" name="email" class="form-control col-md-12" placeholder="* Correo Electrónico" required style="width: 100%;">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="tel" id="usuario" name="usuario" class="form-control " placeholder="* Usuario" required style="width: 100%;">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="password" id="password" name="password" class="form-control " placeholder="* Contraseña" required style="width: 100%;">
                                    </div>
                                </div>
                                <p style="font-size: 15px; color: white;">* Campos Obligatorios</p>
                                <center>
                                    <button type="submit" value="Guardar" class="btn btn-primary"style="width: 220px; height: 50px; border: black 1px solid; margin-right: 15px; border-radius: 5px; color: black; font-size: 16px; font-weight: bold; background: #CFCFCF;">Guardar</button>
                                   
                                </center>
                                <br>
                            </form>
                        </div>
                    </div>
            </fieldset>
    </div>


    <footer class="w3-container w3-center w3-opacity">
        <div class="w3-xlarge w3-padding-32">
            <a class="fa fa-facebook-official w3-hover-opacity" href="https://es-la.facebook.com/UniremingtonOficial/" target="_blank" style="color: black"></a>
            <a class="fa fa-instagram w3-hover-opacity" href="https://www.instagram.com/uniremington_medellin/" target="_blank" style="color: black"></a>
            <a class="fa fa-twitter w3-hover-opacity" href="https://twitter.com/Uni_Remington?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor" target="_blank" style="color: black"></a>
            <a class="fa fa-linkedin w3-hover-opacity" href="https://co.linkedin.com/company/uniremington" target="_blank" style="color: black"></a>
        </div>
        <div class="row">
            <div class="col-xs-12 col-ms-9 col-md-12 col-lg-12" id="grad">
                <center>
                    <h5 style="font-weight: bold;">www.uniremington.edu.co <br>
                        Medellín Antioquia, Colombia<br>
                        &copy;<br>
                        Acosta A., Castro A. & Mora N.</h5>
                </center><br>
            </div>
        </div>
    </footer>



    <script type="text/javascript" src="../recursos/jsLibrerias/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="../recursos/jsLibrerias/popper.min.js"></script>
    <script type="text/javascript" src="../recursos/jsLibrerias/bootstrap.min.js"></script>

</body>

</html>