<?php
require_once '../control/UsuarioControl.php';
$usuarioControl = new UsuarioControl();
$usu =$usuarioControl->obtenerPorId($_REQUEST['idusuario']);

require_once '../control/PerfilControl.php';
$perfilControl = new PerfilControl();

?>


<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
            <meta charset="UTF-8" >
            
                              
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">

              
        <style>
            
            
            
            
            body{
                    background-image: url(img/fondo.jpg);
                    background-size: cover;
                    background-repeat: no-repeat;
                    background-attachment:fixed;
            }
            
            .borde {
                border: 2px solid red;
                border-collapse: collapse;
            }

            h3 {
                color: white;
                font-family: Helvetica;
            }
            
           
            
            
            .contenedor{
                width: 450px;
                margin: 0 auto;
                font-family: Calibri;
            }
            
           
             .formulario{
        transition: 2s;
        margin-top: 20px;
        width: 35%;
        box-shadow: 0px 0px 80px black,0px 0px 80px black ;
        background-color: rgba(255,255,255 ,.8);
        background-color: white;
                color: black;
                border: 3px solid gray;
                padding: 1px 20px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
    }

    .formulario:hover{
        transition: .8s;
        background-color:rgba(255,255,255 ,.7);

    }
    
    
    tr > td:first-child{
                font-weight: bold;
                font-size: 17px;
                font-style: Helvetica;
            }
            
            tr > td:nth-child(2){
                font-style: italic;
                color: #424242;
                font-size: 18px;
            }
           
    
            
          
           
            
              .logo{
                height: 140px;
                margin-top: 20px;
                background: center;
            }
            
            
            .simple{
                text-decoration: none;
                color: white;
                  
            }
            
            a:hover{
                  color: white;
                 
            }
            
            
           
          
        </style>        
    </head>
    <body>
        
         <nav class="navbar navbar-expand-sm bg-dark navbar-dark ">
  <!-- Brand/logo -->
  <a class="navbar-brand" href="home.php">
    <img src="img/logo.png" alt="logo" style="width:70px;" class="rounded-circle" alt="Cinque Terre" width="90" height="56">
  </a>
  
  <!-- Links -->
  <ul class="navbar-nav ">
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
        Maestros
      </a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="../vista/estudianteListar.php">Estudiantes</a>
        <a class="dropdown-item" href="../vista/docenteListar.php">Docentes</a>
        <a class="dropdown-item" href="../vista/especialidadListar.php">Especialidades</a>
        <a class="dropdown-item" href="../vista/cursoListar.php">Cursos</a>
        <a class="dropdown-item" href="../vista/ciudadListar.php">Ciudad</a>
      </div>
    </li>

    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
        Proceso
      </a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="../vista/registro_cursoListar.php">Registro_Curso</a>
        
      </div>
    </li>

    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
        Salida
      </a>
      <div class="dropdown-menu">


         <a class="dropdown-item" href="../Informes/pdf/reporte3.1.php">Informe Estudiantes por Curso Pdf</a> 
         <a class="dropdown-item" href="../Informes/pdf/reporte3.2.php">Informe Estudiantes por Curso Excel</a>      
        <a class="dropdown-item" href="../Informes/pdf/reporte3.3.php">Informe Estudiantes Excel</a>
        <a class="dropdown-item" href="../Informes/pdf/reporte3.4.php">Informe Docentes Excel</a>
        <a class="dropdown-item" href="../Informes/pdf/reporte3.5.php">Informe Cursos Excel</a>
        <a class="dropdown-item" href="../Informes/pdf/reporte3.6.php">Informe Registros_Cursos Excel</a>
        <a class="dropdown-item" href="../Informes/pdf/reporte3.7.php">Informe Especialidades Excel</a>
        <a class="dropdown-item" href="../Informes/pdf/reporte3.8.php">Informe Ciudades Excel</a>
      </div>
    </li>

     <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
        Configuración
      </a>
      <div class="dropdown-menu">
        
        <a class="dropdown-item" href="../vista/usuarioListar.php">Usuario</a>
        <a class="dropdown-item" href="../vista/perfilListar.php">Perfil</a>
      </div>
    </li>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    
   
</div>
  </ul>

</nav>
        <div class="row">
       <div class="color1 col-xs-12 col-ms-9 col-md-12 col-lg-12"> 
        
        
        
        <center><div class="container formulario">
                <center><img src="img/ejecutivo_logo.png" class="logo center-block"></center>
                <br>
                </br>
             <h2> Consultar Usuario</h2>
             <br>
             </br>
        <table>
            <tr>
                <td>Código</td>
                <td><?php echo $usu->getIdusuario(); ?></td>
            </tr>
            
            <tr>
                <td>Usuario</td>
                <td><?php echo $usu->getUsuario();?></td>
            </tr>
            
            <tr>
                <td>Contraseña</td>
                <td><?php echo $usu->getPassword();?></td>
            </tr>           
           
            
             <tr>
                <td>Perfil</td>
                <td><?php echo $usu->getPerfil()->descripcionp; ?></td>
            </tr>
            
                
             <tr>
                <td>Estado</td>
                <td><?php echo $usu->getEstado();?></td>
            </tr>
        </table>
            
            <br/>
            <button type="submit" class="btn btn-dark center-block"> <a class="simple hover"  href="javascript:history.back()"> Regresar</a></button>  
            <br>
            </br>
             <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="js/popper.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    
            </div></center>
       </div>
                         
    </body>
</html>
