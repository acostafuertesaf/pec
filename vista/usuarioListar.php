<?php
require_once '../control/UsuarioControl.php';
$usuarioControl =new UsuarioControl();



require_once '../conexion/Db.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
            <meta charset="UTF-8" >
            <title>Listar Usuario</title>

            <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/bootstrap-theme.css" rel="stylesheet">
        <link href="css/jquery.dataTables.min.css" rel="stylesheet">
        <script src="js/jquery-3.1.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script> 
        <script src="js/jquery.dataTables.min.js"></script>
            
                              
     

                               
        <style>
            .contenedor{
                width: 1200px;
                margin: 0 auto;
                font-family: Helvetica;
                align-content: center;
                
                
            }
            
            body{
                    background-image: url(img/fondo.jpg);
                    background-size: cover;
                    background-repeat: no-repeat;
                    background-attachment:fixed;
            }
            
            .borde {
                border: 2px solid red;
                border-collapse: collapse;
            }

            h3 {
                color: white;
                font-family: Helvetica;
            }
            
            .logo{
                height: 110px;
                margin-top: 20px;
                background: center;
            }
            .table-bordered{
                background-color: white;
                width: 600px;
                height: 200px;
            }
            
            .table-fila1{
                background-color: #424242;
                color: white;
                width: 200px;
                height: 40px;
            }
            
#searchTerm{
    width: 300px;
    height: 40px;
    font-size: 17px;
    text-indent: 10px;
    background: white;
    color: black;
    border-color: #424242;

}
            
            
        </style>
    </head>
   
    <body>
        
  <nav class="navbar navbar-expand-sm bg-dark navbar-dark  ">
  <!-- Brand/logo -->
  <a class="navbar-brand" href="home.php">
    <img src="img/logo.png" alt="logo" style="width:70px;" class="rounded-circle" alt="Cinque Terre" width="90" height="56">
  </a>
  
  <!-- Links -->
  <ul class="navbar-nav ">
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
        Maestros
      </a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="../vista/estudianteListar.php">Estudiantes</a>
        <a class="dropdown-item" href="../vista/docenteListar.php">Docentes</a>
        <a class="dropdown-item" href="../vista/especialidadListar.php">Especialidades</a>
        <a class="dropdown-item" href="../vista/cursoListar.php">Cursos</a>
        <a class="dropdown-item" href="../vista/ciudadListar.php">Ciudad</a>
      </div>
    </li>

    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
        Proceso
      </a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="../vista/registro_cursoListar.php">Registro_Curso</a>
        
      </div>
    </li>

    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
        Salida
      </a>
     <div class="dropdown-menu">


         <a class="dropdown-item" href="../Informes/pdf/reporte3.1.php">Informe Estudiantes por Curso Pdf</a> 
         <a class="dropdown-item" href="../Informes/pdf/reporte3.2.php">Informe Estudiantes por Curso Excel</a>      
        <a class="dropdown-item" href="../Informes/pdf/reporte3.3.php">Informe Estudiantes Excel</a>
        <a class="dropdown-item" href="../Informes/pdf/reporte3.4.php">Informe Docentes Excel</a>
        <a class="dropdown-item" href="../Informes/pdf/reporte3.5.php">Informe Cursos Excel</a>
        <a class="dropdown-item" href="../Informes/pdf/reporte3.6.php">Informe Registros_Cursos Excel</a>
        <a class="dropdown-item" href="../Informes/pdf/reporte3.7.php">Informe Especialidades Excel</a>
        <a class="dropdown-item" href="../Informes/pdf/reporte3.8.php">Informe Ciudades Excel</a>
      </div>
    </li>

     <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
        Configuración
      </a>
      <div class="dropdown-menu">
        
        <a class="dropdown-item" href="../vista/usuarioListar.php">Usuario</a>
        <a class="dropdown-item" href="../vista/perfilListar.php">Perfil</a>
      </div>
    </li>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="" id="navbardrop" data-toggle="dropdown">
       Sesion
      </a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="../vista/php/logout.php">Salir</a>
        
      </div>
    </li>
    
   
</div>
  </ul>

</nav>

        <script language="javascript">
            function doSearch() {
                var tableReg = document.getElementById('regTable');
                var searchText = document.getElementById('searchTerm').value.toLowerCase();
                for (var i = 1; i < tableReg.rows.length; i++) {
                    var cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
                    var found = false;
                    for (var j = 0; j < cellsOfRow.length && !found; j++) {
                        var compareWith = cellsOfRow[j].innerHTML.toLowerCase();
                        if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1)) {
                            found = true;
                        }
                    }
                    if (found) {
                        tableReg.rows[i].style.display = '';
                    } else {
                        tableReg.rows[i].style.display = 'none';
                    }
                }
            }
        </script>

      
       
        
        <div class="contenedor">
            <table class=" table-bordered table-hover" id="regTable">
                
            <div class="row">
               
                <img src="img/ejecutivo_logo.png" class="logo center-block">
                    
                </div>
                &nbsp;  &nbsp;
            
        </div>


            <h3>Listado Usuarios:</h3>
             &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;  &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;  &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <input id="searchTerm" type="text"  placeholder="Buscar Aqui.." onkeyup="doSearch()" /> 
            

           
                    
                 <div class="marDer">
                      <a href="usuarioAgregar.php"><span><img src="../recursos/img/add1.png" width="34" height="34" title="Agregar Usuario..."/></span></a>&nbsp;
                        
                    </div>
    &nbsp;  
        

            <tr class="table-fila1"  align="center">

                    
                    <th class="borde marInt">Código</th>
                    <th class="borde marInt">Usuario</th>
                    
                    <th class="borde marInt">Perfil</th>
                    <th class="borde marInt">Estado</th>
                    <th class="borde marInt">Acciones</th>
                    
                </tr>
               
                <?php foreach ($usuarioControl->obtenerTodos() as $usu) { ?>
                   
                <tr class="selFila" id="selFila">
                    <td class="borde marInt alCen"><?php echo $usu->getIdusuario(); ?></td>
                    <td class="borde marInt alCen"><?php echo $usu->getUsuario(); ?> </td>
                    
            <td class="borde marInt alCen"><?php echo $usu->getPerfil()->descripcionp;?></td>
                    <td class="borde marInt alCen"><?php echo $usu->getEstado(); ?> </td>
                
                    
                    
                    <td class="borde marInt">
                        <a href="usuarioConsultar.php?idusuario=<?php echo $usu->getIdusuario(); ?>"><span><img src="../recursos/img/view2.png" width="16" height="16" title="Ver Detalles.."/></span></a>&nbsp;
                        <a href="usuarioModificar.php?idusuario=<?php echo $usu->getIdusuario(); ?>"><span><img src="../recursos/img/edit2.png" width="16" height="16" title="Modificar.."/></span></a>&nbsp;
                        <a href="../funciones/eliminarUsuario.php?idusuario=<?php echo $usu->getIdusuario(); ?>"><span><img src="../recursos/img/del2.png" width="16" height="16" title="Eliminar.."/></span></a>&nbsp;
                        
                    </td> 
                    
                    
                </tr>
                <?php } ?>
                </div>
                
            
           </tr>
           </table>
        </div>
        


        
    <script src="js/search.js"></script> 
    <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="js/popper.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>


    </body>
</html>

