<?php
require_once '../control/UsuarioControl.php';
$usuarioControl = new UsuarioControl();
$usu = $usuarioControl->obtenerPorId($_REQUEST['idusuario']);


require_once '../control/PerfilControl.php';
$perfilControl = new PerfilControl();


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta charset="UTF-8">

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Modificar Perfil</title>


  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">


</head>

<style>
  body {
    background-image: url(img/fondo.jpg);
    background-size: cover;
    background-repeat: no-repeat;
    background-attachment: fixed;
  }

  .formulario {
    transition: 2s;
    margin-top: 20px;
    width: 60%;
    box-shadow: 0px 0px 80px black, 0px 0px 80px black;
    background-color: rgba(255, 255, 255, .6);
  }

  .formulario:hover {
    transition: .8s;
    background-color: rgba(255, 255, 255, .7);
  }


  .logo {
    height: 120px;
    margin-top: 20px;
    background: center;
  }

  .espaciado {
    margin-top: .5px;
  }

  fieldset {
    transition: 2s;
    margin-bottom: 30px;
    border-color: rgba(213, 0, 0, 1);
    border-style: groove;
    border-width: 5px;
    border-radius: 80px;
  }

  h3,
  h4 {
    color: black;
    text-align: center;
    font-family: Helvetica;
  }

  .Input {
    transition: .8s;
    background-color: rgba(255, 255, 255, .255);
    color: black;
    border-color: #006;
    border-bottom-color: black;
    border-bottom-style: groove;
    border-left: none;
    border-right: none;
    border-top: none;
    border-width: 4px;
  }

  .Input:hover {
    transition: .8s;
    background-color: rgba(55, 71, 79, .5);
    box-shadow: inset;
    border-bottom-color: red;
  }

  .simple {
    text-decoration: none;
    color: white;
  }

  .simple:hover {
    color: white;
    text-decoration: none;
  }

  .Input:focus {
    transition: .8s;
    border-bottom-color: red;
  }

  p {
    color: black;
    font-family: Helvetica;
    text-align: left;
  }

  .label {
    text-align: left;
  }


  @media screen and (max-width:1050px) {
    .logo {
      height: 120px;
    }

    .formulario {
      transition: 2s;
      width: 100%;
      margin-top: 10px;
    }
  }
</style>
<script src="../recursos/js/script.js"></script>

<body>

  <nav class="navbar navbar-expand-sm bg-dark navbar-dark ">
    <!-- Brand/logo -->
    <a class="navbar-brand" href="home.php">
      <img src="img/logo.png" alt="logo" style="width:70px;" class="rounded-circle" alt="Cinque Terre" width="90" height="56">
    </a>

    <!-- Links -->
    <ul class="navbar-nav ">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
          Maestros
        </a>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="../vista/estudianteListar.php">Estudiantes</a>
          <a class="dropdown-item" href="../vista/docenteListar.php">Docentes</a>
          <a class="dropdown-item" href="../vista/especialidadListar.php">Especialidades</a>
          <a class="dropdown-item" href="../vista/cursoListar.php">Cursos</a>
          <a class="dropdown-item" href="../vista/ciudadListar.php">Ciudad</a>
        </div>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
          Proceso
        </a>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="../vista/registro_cursoListar.php">Registro_Curso</a>

        </div>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
          Salida
        </a>
        <div class="dropdown-menu">


          <a class="dropdown-item" href="../Informes/pdf/reporte3.1.php">Informe Estudiantes por Curso Pdf</a>
          <a class="dropdown-item" href="../Informes/pdf/reporte3.2.php">Informe Estudiantes por Curso Excel</a>
          <a class="dropdown-item" href="../Informes/pdf/reporte3.3.php">Informe Estudiantes Excel</a>
          <a class="dropdown-item" href="../Informes/pdf/reporte3.4.php">Informe Docentes Excel</a>
          <a class="dropdown-item" href="../Informes/pdf/reporte3.5.php">Informe Cursos Excel</a>
          <a class="dropdown-item" href="../Informes/pdf/reporte3.6.php">Informe Registros_Cursos Excel</a>
          <a class="dropdown-item" href="../Informes/pdf/reporte3.7.php">Informe Especialidades Excel</a>
          <a class="dropdown-item" href="../Informes/pdf/reporte3.8.php">Informe Ciudades Excel</a>
        </div>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
          Configuración
        </a>
        <div class="dropdown-menu">

          <a class="dropdown-item" href="../vista/usuarioListar.php">Usuario</a>
          <a class="dropdown-item" href="../vista/perfilListar.php">Perfil</a>
        </div>
      </li>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;


      </div>
    </ul>

  </nav>

  <div class="container formulario">
    <form onsubmit="return validar()" action="../funciones/modificarUsuario.php" method="get">


      <center><img src="img/ejecutivo_logo.png" class="logo center-block"></center>
      <div class="row">
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;


      </div>

      <center>
        <fieldset class="col-xs-9 col-md-12">

          <legend class="hidden-xs">
            <h3>Modificar Usuario</h3>

          </legend>


          <div class="row">
            <div class=" col-xs-12 col-ms-9 col-md-6">

              <label for="idusuario">
                <h4>Código *</h4>
              </label>
              <div class="col-md-12">

                <input type="text" id="idusuario" class="form-control" name="idusuario" value="<?php echo $usu->getIdusuario(); ?>" readonly>

              </div>
            </div>


            <div class=" col-xs-12 col-ms-9 col-md-6">
              <label for="perfil">
                <h4>Perfil *</h4>
              </label>
              <div class="col-md-12">

                <select class="form-control" id="idperfil" name="idperfil">
                  <?php foreach ($perfilControl->obtenerTodos() as $per) { ?>
                    <option value="<?php echo $per->idperfil; ?>" <?php echo ($usu->getIdusuario() == $per->idperfil ? "selected" : "") ?>>
                      <?php echo $per->descripcionp; ?>
                    </option>
                  <?php } ?>
                </select>

              </div>

            </div>
          </div>


          &nbsp;



          <div class="row">
            <div class=" col-xs-12 col-ms-9 col-md-6">
              <label for="usuario">
                <h4>Usuario *</h4>
              </label>
              <div class="col-md-12">

                <input type="text" id="usuario" name="usuario" class="form-control" value="<?php echo $usu->getUsuario(); ?>">

              </div>
            </div>




            <div class=" col-xs-12 col-ms-9 col-md-6">
              <label for="password">
                <h4>Contraseña * </h4>
              </label>
              <div class="col-md-12">
                <input type="password" id="password" name="password" class="form-control " value="<?php echo $usu->getPassword(); ?>">
              </div>

            </div>
          </div>

          &nbsp;

          <div class="row">
            <div class=" col-xs-12 col-ms-9 col-md-6">
              <label for="estado">
                <h4>Estado</h4>
              </label>
              <div class="col-md-12">

                <input type="text" id="estado" name="estado" class="form-control" value="<?php echo $usu->getEstado(); ?>">

              </div>
            </div>


          </div>
          &nbsp;




          &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;
          <p>* Campos Obligatorios</p>



          <center><button type="submit" value="Guardar" class="btn btn-danger">Guardar</button>
            &nbsp;
            <button type="submit" class="btn btn-danger center-block"> <a class="simple hover" href="javascript:history.back()"> Regresar</a></button> </center>
          &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;



        </fieldset>
      </center>

  </div>
  </div>


  <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
  <script type="text/javascript" src="js/popper.min.js"></script>
  <script type="text/javascript" src="js/bootstrap.min.js"></script>

</body>

</html>